use crate::executor::Registry;
use core::future::Future;
use core::pin::Pin;
use core::task::Context;
use core::task::Poll;
use core::time::Duration;
use std::io::Result as IoResult;
use std::time::Instant;

pub struct Delay {
    registry: Registry,
    at: Option<Instant>,
    delay: Duration,
}

impl Delay {
    pub fn new(registry: Registry, delay: Duration) -> Delay {
        Delay {
            registry,
            delay,
            at: None,
        }
    }
}

impl Future for Delay {
    type Output = IoResult<()>;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let now = Instant::now();

        match self.at {
            Some(at) => {
                if now >= at {
                    Poll::Ready(Ok(()))
                } else {
                    self.registry.timer(ctx, at)?;
                    Poll::Pending
                }
            }
            None => {
                let at = now + self.delay;
                self.at = Some(at);
                self.registry.timer(ctx, at)?;
                Poll::Pending
            }
        }
    }
}
