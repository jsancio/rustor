use crate::sys::executor::Interest;
use crate::sys::executor::Registry;
use crate::sys::net::AsyncTcpStream;
use core::pin::Pin;
use core::task::Context;
use core::task::Poll;
use futures::stream::Stream;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Result as IoResult;
use std::net::TcpListener;
use std::net::ToSocketAddrs;
use std::os::unix::io::AsRawFd;

pub struct AsyncTcpListener {
    reactor: Registry,
    token: u64,
    listener: TcpListener,
}

impl AsyncTcpListener {
    fn new(reactor: Registry, listener: TcpListener) -> IoResult<AsyncTcpListener> {
        listener.set_nonblocking(true)?;

        let token = reactor.add_fd(listener.as_raw_fd())?;

        Ok(AsyncTcpListener {
            reactor,
            token,
            listener,
        })
    }

    pub fn bind<T>(reactor: Registry, addrs: T) -> IoResult<AsyncTcpListener>
    where
        T: ToSocketAddrs,
    {
        let listener = TcpListener::bind(addrs)?;

        AsyncTcpListener::new(reactor, listener)
    }
}

impl Stream for AsyncTcpListener {
    type Item = Result<AsyncTcpStream, Error>;

    fn poll_next(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        match self.listener.accept() {
            Err(ref err) if err.kind() == ErrorKind::WouldBlock => {
                match self.reactor.modify_fd(self.token, Interest::Read, ctx) {
                    Err(err) => Poll::Ready(Some(Err(err))),
                    Ok(()) => Poll::Pending,
                }
            }
            Err(err) => Poll::Ready(Some(Err(err))),
            Ok((connection, _)) => {
                let stream = AsyncTcpStream::new(self.reactor.clone(), connection);
                Poll::Ready(Some(stream))
            }
        }
    }
}

impl Drop for AsyncTcpListener {
    fn drop(&mut self) {
        // Ignore errors
        let _ = self.reactor.delete_fd(self.token);
    }
}
