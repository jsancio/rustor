use crate::buffer::Buf;
use crate::buffer::BufMut;
use crate::buffer::ReadView;
use crate::buffer::WriteView;
use crate::io::AsyncRead;
use crate::io::AsyncWrite;
use crate::io::Error;
use crate::sys::executor::Interest;
use crate::sys::executor::Registry;
use core::future::Future;
use core::mem::MaybeUninit;
use core::pin::Pin;
use core::task::Context;
use core::task::Poll;
use futures::ready;
use libc::EINPROGRESS;
use socket2::Domain;
use socket2::Socket;
use socket2::Type;
use std::io::ErrorKind;
use std::io::Read;
use std::io::Result as IoResult;
use std::io::Write;
use std::net::SocketAddr;
use std::net::TcpStream;
use std::net::ToSocketAddrs;
use std::os::unix::io::AsRawFd;

pub struct AsyncTcpStream {
    registry: Registry,
    read_state: Option<WriteView>,
    stream: TcpStream,
    token: u64,
    write_state: Option<ReadView>,
}

impl AsyncTcpStream {
    pub(crate) fn new(registry: Registry, stream: TcpStream) -> IoResult<AsyncTcpStream> {
        stream.set_nonblocking(true)?;

        let token = registry.add_fd(stream.as_raw_fd())?;

        Ok(AsyncTcpStream::connected(registry, stream, token))
    }

    pub fn connect<T>(registry: Registry, addrs: T) -> ConnectFuture<T>
    where
        T: ToSocketAddrs,
    {
        ConnectFuture::new(registry, addrs)
    }

    fn connected(registry: Registry, stream: TcpStream, token: u64) -> AsyncTcpStream {
        AsyncTcpStream {
            registry,
            read_state: None,
            stream,
            token,
            write_state: None,
        }
    }
}

impl AsyncRead for AsyncTcpStream {
    fn poll_read_ready(
        &mut self,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<Option<(usize, WriteView)>, Error<WriteView>>> {
        if let Some(mut buffer) = self.read_state.take() {
            // TODO: The long term solution is to implement a version of `self.stream.read` that
            // takes a slice of `MaybeUninit<u8>`.
            // safety: calling `slide_get_mut` is okay because `self.stream.read` only writes to
            // the slice.
            match self
                .stream
                .read(unsafe { MaybeUninit::slice_get_mut(buffer.write_mut_slice()) })
            {
                Ok(size) => {
                    // safety: safe to advance by `size` bytes because `read` was able to copy that
                    // many bytes to the slice.
                    unsafe {
                        buffer.advance_write_index(size);
                    }
                    Poll::Ready(Ok(Some((size, buffer))))
                }
                Err(ref err) if err.kind() == ErrorKind::WouldBlock => {
                    match self.registry.modify_fd(self.token, Interest::Read, ctx) {
                        Err(err) => Poll::Ready(Err(Error::new(buffer, err))),
                        Ok(()) => {
                            self.read_state = Some(buffer);
                            Poll::Pending
                        }
                    }
                }
                Err(err) => Poll::Ready(Err(Error::new(buffer, err))),
            }
        } else {
            Poll::Ready(Ok(None))
        }
    }

    fn poll_read(
        &mut self,
        ctx: &mut Context<'_>,
        buffer: WriteView,
    ) -> Poll<Result<(usize, WriteView), Error<WriteView>>> {
        if self.read_state.is_none() {
            self.read_state = Some(buffer);
            match ready!(self.poll_read_ready(ctx)) {
                Ok(Some(result)) => Poll::Ready(Ok(result)),
                Err(err) => Poll::Ready(Err(err)),
                Ok(None) => unreachable!(), // Cannot return None because we set Some(buffer)
            }
        } else {
            Poll::Ready(Err(Error::new(
                buffer,
                std::io::Error::new(
                    ErrorKind::Other,
                    "Error reading buffer, AsyncTcpStream is full",
                ),
            )))
        }
    }
}

impl AsyncWrite for AsyncTcpStream {
    fn poll_write_ready(
        &mut self,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<Option<(usize, ReadView)>, Error<ReadView>>> {
        if let Some(mut buffer) = self.write_state.take() {
            match self.stream.write(buffer.read_slice()) {
                Ok(size) => {
                    buffer.advance_read_index(size);
                    Poll::Ready(Ok(Some((size, buffer))))
                }
                Err(ref err) if err.kind() == ErrorKind::WouldBlock => {
                    match self.registry.modify_fd(self.token, Interest::Write, ctx) {
                        Err(err) => Poll::Ready(Err(Error::new(buffer, err))),
                        Ok(()) => {
                            self.write_state = Some(buffer);
                            Poll::Pending
                        }
                    }
                }
                Err(err) => Poll::Ready(Err(Error::new(buffer, err))),
            }
        } else {
            Poll::Ready(Ok(None))
        }
    }

    fn poll_write(
        &mut self,
        ctx: &mut Context<'_>,
        buffer: ReadView,
    ) -> Poll<Result<(usize, ReadView), Error<ReadView>>> {
        if self.write_state.is_none() {
            self.write_state = Some(buffer);
            match ready!(self.poll_write_ready(ctx)) {
                Ok(Some(result)) => Poll::Ready(Ok(result)),
                Err(err) => Poll::Ready(Err(err)),
                Ok(None) => unreachable!(),
            }
        } else {
            Poll::Ready(Err(Error::new(
                buffer,
                std::io::Error::new(
                    ErrorKind::Other,
                    "Error writing buffer, AsyncTcpStream is full",
                ),
            )))
        }
    }
}

impl Drop for AsyncTcpStream {
    fn drop(&mut self) {
        // Ignore errors
        let _ = self.registry.delete_fd(self.token);
    }
}

pub struct ConnectFuture<T> {
    addrs: T,
    registry: Registry,
    state: Option<(u64, Socket)>,
}

impl<T> ConnectFuture<T> {
    fn new(registry: Registry, addrs: T) -> Self {
        ConnectFuture {
            addrs,
            registry,
            state: None,
        }
    }
}

impl<T> Future for ConnectFuture<T>
where
    T: ToSocketAddrs + Unpin,
{
    type Output = IoResult<AsyncTcpStream>;

    fn poll(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        if let Some((token, socket)) = this.state.take() {
            match socket.take_error()? {
                Some(err) => Poll::Ready(Err(err)),
                None => Poll::Ready(Ok(AsyncTcpStream::connected(
                    this.registry.clone(),
                    socket.into_tcp_stream(),
                    token,
                ))),
            }
        } else {
            let addrs = this.addrs.to_socket_addrs()?;

            let mut last_err = None;
            for addr in addrs {
                let domain = match addr {
                    SocketAddr::V4(..) => Domain::ipv4(),
                    SocketAddr::V6(..) => Domain::ipv6(),
                };

                let socket = Socket::new(domain, Type::stream(), None)?;
                socket.set_nonblocking(true)?;

                let token = this.registry.add_fd(socket.as_raw_fd())?;

                match socket.connect(&addr.into()) {
                    Ok(()) => {
                        return Poll::Ready(Ok(AsyncTcpStream::connected(
                            this.registry.clone(),
                            socket.into_tcp_stream(),
                            token,
                        )))
                    }
                    Err(ref err) if err.raw_os_error() == Some(EINPROGRESS) => {
                        match this.registry.modify_fd(token, Interest::Write, ctx) {
                            Err(err) => {
                                this.registry.delete_fd(token)?;
                                return Poll::Ready(Err(err));
                            }
                            Ok(()) => {
                                this.state = Some((token, socket));
                                return Poll::Pending;
                            }
                        }
                    }
                    Err(err) => {
                        this.registry.delete_fd(token)?;
                        last_err = Some(err)
                    }
                }
            }

            Poll::Ready(Err(last_err.unwrap_or_else(|| {
                std::io::Error::new(ErrorKind::InvalidInput, "could not resolve to any address")
            })))
        }
    }
}
