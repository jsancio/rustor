use crate::executor::Timer;
use core::task::Context;
use core::task::Waker;
use core::time::Duration;
use epoll;
use slab::Slab;
use std::cell::RefCell;
use std::convert::TryInto;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Result as IoResult;
use std::os::unix::io::RawFd;
use std::rc::Weak;
use std::time::Instant;

#[derive(Debug)]
struct Task {
    fd: RawFd,
    read: Option<Waker>,
    write: Option<Waker>,
}

impl Task {
    fn new(fd: RawFd) -> Task {
        Task {
            fd,
            read: None,
            write: None,
        }
    }
}

pub(crate) struct Reactor {
    epoll: RawFd,
    evented_wakers: RefCell<Slab<Task>>,
    timer: Timer,
}

impl Reactor {
    pub(crate) fn new() -> IoResult<Self> {
        let epoll_fd = epoll::create(true)?;
        Ok(Reactor {
            epoll: epoll_fd,
            evented_wakers: RefCell::new(Slab::new()),
            timer: Timer::new(),
        })
    }

    pub(crate) fn wake_on_timer(&self) -> (bool, Option<Instant>) {
        self.timer.wake_on_timer()
    }

    pub(crate) fn wake_on_io(&self, delay: Option<Duration>) -> IoResult<()> {
        // TODO: We don't need to malloc every time if we store this in the struct
        let mut events = vec![epoll::Event::new(epoll::Events::empty(), 0); 1024];

        let size = epoll::wait(
            self.epoll,
            delay
                .map(|delay| {
                    delay
                        .as_millis()
                        .try_into()
                        .unwrap_or_else(|_| std::i32::MAX)
                })
                .unwrap_or_else(|| -1),
            &mut events[..],
        )?;

        let triggers = &events[..size];

        for event in triggers.iter() {
            let index = event.data.try_into().map_err(|_| {
                // TODO: return event loop specific error
                let data = event.data;
                Error::new(
                    ErrorKind::Other,
                    format!("Unable to convert u64 ({}) to a usize", data),
                )
            })?;

            match self.evented_wakers.borrow_mut().get_mut(index) {
                Some(task) => {
                    let active = epoll::Events::from_bits_truncate(event.events);

                    if active.contains(epoll::Events::EPOLLIN) {
                        if let Some(waker) = task.read.take() {
                            waker.wake();
                        }
                    }

                    if active.contains(epoll::Events::EPOLLOUT) {
                        if let Some(waker) = task.write.take() {
                            waker.wake();
                        }
                    }
                }
                None => {
                    eprintln!("Reactor got an event for an file which is not registered");
                }
            }
        }

        Ok(())
    }

    fn add_fd(&self, fd: RawFd) -> IoResult<u64> {
        let index = self.evented_wakers.borrow_mut().insert(Task::new(fd));

        let id: u64 = index.try_into().map_err(|_| {
            // TODO: This pattern is not great look for alternatives
            self.evented_wakers.borrow_mut().remove(index);
            // TODO: return event loop specific error
            Error::new(
                ErrorKind::Other,
                format!("Unable to convert usize ({}) to a u64", index),
            )
        })?;

        let mut events = epoll::Events::EPOLLET;
        events.insert(epoll::Events::EPOLLIN);
        events.insert(epoll::Events::EPOLLOUT);

        epoll::ctl(
            self.epoll,
            epoll::ControlOptions::EPOLL_CTL_ADD,
            fd,
            epoll::Event::new(events, id),
        )
        .map_err(|err| {
            self.evented_wakers.borrow_mut().remove(index);
            err
        })?;

        Ok(id)
    }

    fn delete_fd(&self, id: u64) -> IoResult<()> {
        let index = id.try_into().map_err(|_| {
            // TODO: return event loop specific error
            Error::new(
                ErrorKind::Other,
                format!("Unable to convert u64 ({}) to a usize", id),
            )
        })?;

        let task = self.evented_wakers.borrow_mut().remove(index);

        epoll::ctl(
            self.epoll,
            epoll::ControlOptions::EPOLL_CTL_DEL,
            task.fd,
            epoll::Event::new(epoll::Events::empty(), Default::default()),
        )
    }

    fn modify_fd(&self, id: u64, interest: Interest, ctx: &mut Context<'_>) -> IoResult<()> {
        let index = id.try_into().map_err(|_| {
            // TODO: return event loop specific error
            Error::new(
                ErrorKind::Other,
                format!("Unable to convert u64 ({}) to a usize", id),
            )
        })?;

        if let Some(task) = self.evented_wakers.borrow_mut().get_mut(index) {
            match interest {
                Interest::Read => task.read = Some(ctx.waker().clone()),
                Interest::Write => task.write = Some(ctx.waker().clone()),
            }

            Ok(())
        } else {
            // TODO: We should not panic and instead return an error
            panic!("Token {} not found in the event loop", index);
        }
    }
}

pub(crate) enum Interest {
    Read,
    Write,
}

#[derive(Clone)]
pub struct Registry {
    reactor: Weak<Reactor>,
}

impl Registry {
    pub(crate) fn new(reactor: Weak<Reactor>) -> Self {
        Registry { reactor }
    }

    pub(crate) fn add_fd(&self, fd: RawFd) -> IoResult<u64> {
        let reactor = self
            .reactor
            .upgrade()
            .ok_or(Error::new(ErrorKind::Other, "Event loop was shutdown"))?;

        reactor.add_fd(fd)
    }

    pub(crate) fn delete_fd(&self, id: u64) -> IoResult<()> {
        let reactor = self
            .reactor
            .upgrade()
            .ok_or(Error::new(ErrorKind::Other, "Event loop was shutdown"))?;

        reactor.delete_fd(id)
    }

    pub(crate) fn modify_fd(
        &self,
        id: u64,
        interest: Interest,
        ctx: &mut Context<'_>,
    ) -> IoResult<()> {
        let reactor = self
            .reactor
            .upgrade()
            .ok_or(Error::new(ErrorKind::Other, "Event loop was shutdown"))?;

        reactor.modify_fd(id, interest, ctx)
    }

    pub(crate) fn timer(&self, ctx: &mut Context<'_>, at: Instant) -> IoResult<()> {
        self.reactor
            .upgrade()
            .ok_or(Error::new(ErrorKind::Other, "Event loop was shutdown"))
            .map(|reactor| reactor.timer.timer(ctx, at))
    }
}
