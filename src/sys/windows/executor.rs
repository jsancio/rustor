use crate::buffer::ReadView;
use crate::buffer::WriteView;
use crate::executor::Timer;
use crate::io::Error;
use core::cell::RefCell;
use core::mem::replace;
use core::time::Duration;
use miow::iocp::CompletionPort;
use miow::iocp::CompletionStatus;
use miow::net::AcceptAddrsBuf;
use miow::Overlapped;
use slab::Slab;
use std::collections::HashMap;
use std::convert::TryInto;
use std::io::ErrorKind;
use std::io::Result as IoResult;
use std::net::TcpStream;
use std::os::windows::io::AsRawSocket;
use std::rc::Weak;
use std::task::Context;
use std::task::Poll;
use std::task::Waker;
use std::time::Instant;
use winapi::shared::winerror::WAIT_TIMEOUT;
use winapi::um::minwinbase::OVERLAPPED;

#[derive(Debug)]
enum OpState {
    Pending(Waker),
    Transferred,
}

pub(crate) enum Operation {
    Read(WriteView),
    Write(ReadView),
    Accept(TcpStream, Box<AcceptAddrsBuf>),
    Connect(TcpStream),
}

struct Task {
    closed: bool,
    operations: HashMap<*mut OVERLAPPED, (Box<Overlapped>, Operation, OpState)>,
}

impl Task {
    fn new() -> Task {
        Task {
            closed: false,
            operations: Default::default(),
        }
    }

    fn complete(&mut self, overlapped: *mut OVERLAPPED) -> bool {
        match self.operations.get_mut(&overlapped) {
            Some((_, _, value)) => {
                match replace(value, OpState::Transferred) {
                    OpState::Pending(waker) => waker.wake(),
                    other => *value = other,
                }
                true
            }
            None => false,
        }
    }

    fn is_removable(&self) -> bool {
        self.closed
            && self
                .operations
                .iter()
                .all(|(_, (_, _, state))| match state {
                    OpState::Transferred => true,
                    OpState::Pending(_) => false,
                })
    }
}

pub(crate) struct Reactor {
    completion_port: CompletionPort,
    slab: RefCell<Slab<Task>>,
    timer: Timer,
}

impl Reactor {
    pub(crate) fn new() -> IoResult<Self> {
        let completion_port = CompletionPort::new(1)?;
        Ok(Reactor {
            completion_port,
            slab: Default::default(),
            timer: Timer::new(),
        })
    }

    pub(crate) fn wake_on_timer(&self) -> (bool, Option<Instant>) {
        self.timer.wake_on_timer()
    }

    pub(crate) fn wake_on_io(&self, delay: Option<Duration>) -> IoResult<()> {
        let mut statuses = vec![CompletionStatus::zero(); 1024];
        let active_statuses = self
            .completion_port
            .get_many(&mut statuses, delay)
            .or_else(|error| match error.raw_os_error() {
                Some(code) if code == WAIT_TIMEOUT.try_into().unwrap() => Ok(&mut []),
                _ => Err(error),
            })?;

        for status in active_statuses {
            let mut slab = self.slab.borrow_mut();
            let remove = match slab.get_mut(status.token()) {
                Some(task) => {
                    if !task.complete(status.overlapped()) {
                        panic!(
                            "Reactor got an event for token ({}) with unknown overlapped ({:?}). bytes = {}",
                            status.token(),
                            status.overlapped(),
                            status.bytes_transferred()
                        );
                    }

                    task.is_removable()
                }
                None => {
                    panic!(
                        "Reactor got an event for a token ({}) which is not registered. overlapped = {:?}; bytes = {}",
                        status.token(),
                        status.overlapped(),
                        status.bytes_transferred()
                    );
                }
            };

            if remove {
                slab.remove(status.token());
            }
        }

        Ok(())
    }
}

#[derive(Clone)]
pub struct Registry {
    reactor: Weak<Reactor>,
}

impl Registry {
    pub(crate) fn new(reactor: Weak<Reactor>) -> Self {
        Registry { reactor }
    }

    pub(crate) fn add_socket<T>(&mut self, socket: &T) -> IoResult<usize>
    where
        T: AsRawSocket + ?Sized,
    {
        match self.reactor.upgrade() {
            Some(reactor) => {
                let task = Task::new();
                let token = reactor.slab.borrow_mut().insert(task);
                match reactor.completion_port.add_socket(token, socket) {
                    Err(err) => {
                        reactor.slab.borrow_mut().remove(token);
                        return Err(err);
                    }
                    Ok(()) => {}
                };

                Ok(token)
            }

            None => Err(std::io::Error::new(
                ErrorKind::Other,
                "Event lopp was closed",
            )),
        }
    }

    pub(crate) fn remove_socket(&mut self, token: usize) -> IoResult<()> {
        self.reactor
            .upgrade()
            .ok_or(std::io::Error::new(
                ErrorKind::Other,
                "Event loop was closed",
            ))
            .map(|reactor| {
                // Marked it as closed if we have pending operations
                let mut slab = reactor.slab.borrow_mut();
                let remove = slab.get_mut(token).map_or(false, |task| {
                    if task.operations.is_empty() {
                        true
                    } else {
                        task.closed = true;
                        false
                    }
                });

                if remove {
                    slab.remove(token);
                }
            })
    }

    pub(crate) fn poll_completed_operation(
        &mut self,
        _ctx: &mut Context<'_>,
        token: usize,
        overlapped: *mut OVERLAPPED,
    ) -> Poll<IoResult<Option<(Box<Overlapped>, Operation)>>> {
        // TODO: I think we should check that the given Context matches the stored Waker. Or maybe
        // replace it.
        // TODO: simplify this code ASAP!!!
        match self.reactor.upgrade() {
            Some(reactor) => match reactor.slab.borrow_mut().get_mut(token) {
                Some(task) => match task.operations.remove(&overlapped) {
                    Some((overlapped_box, operation, OpState::Pending(waker))) => {
                        task.operations.insert(
                            overlapped,
                            (overlapped_box, operation, OpState::Pending(waker)),
                        );
                        Poll::Pending
                    }
                    Some((overlapped_box, operation, OpState::Transferred)) => {
                        Poll::Ready(Ok(Some((overlapped_box, operation))))
                    }
                    None => Poll::Ready(Ok(None)),
                },
                None => Poll::Ready(Err(std::io::Error::new(
                    ErrorKind::Other,
                    format!("Task for token {} was not registered", token),
                ))),
            },
            None => Poll::Ready(Err(std::io::Error::new(
                ErrorKind::Other,
                "Event loop was closed",
            ))),
        }
    }

    pub(crate) fn pending_read_operation(
        &mut self,
        ctx: &mut Context<'_>,
        token: usize,
        overlapped: Box<Overlapped>,
        buffer: WriteView,
    ) -> Poll<Result<(), Error<WriteView>>> {
        let reactor = match self.reactor.upgrade() {
            Some(reactor) => reactor,
            None => {
                return Poll::Ready(Err(Error::new(
                    buffer,
                    std::io::Error::new(ErrorKind::Other, "Event loop was shutdown"),
                )))
            }
        };

        let mut slab = reactor.slab.borrow_mut();
        let task = match slab.get_mut(token) {
            Some(task) => task,
            None => {
                return Poll::Ready(Err(Error::new(
                    buffer,
                    std::io::Error::new(
                        ErrorKind::Other,
                        format!("Task not found for token {}.", token),
                    ),
                )))
            }
        };

        // TODO: We should check the state of the system. Can only do this when it
        // is Idle
        task.operations.insert(
            overlapped.raw(),
            (
                overlapped,
                Operation::Read(buffer),
                OpState::Pending(ctx.waker().clone()),
            ),
        );
        Poll::Pending
    }

    pub(crate) fn pending_write_operation(
        &mut self,
        ctx: &mut Context<'_>,
        token: usize,
        overlapped: Box<Overlapped>,
        buffer: ReadView,
    ) -> Poll<Result<(), Error<ReadView>>> {
        let reactor = match self.reactor.upgrade() {
            Some(reactor) => reactor,
            None => {
                return Poll::Ready(Err(Error::new(
                    buffer,
                    std::io::Error::new(ErrorKind::Other, "Event loop was shutdown"),
                )))
            }
        };

        let mut slab = reactor.slab.borrow_mut();
        let task = match slab.get_mut(token) {
            Some(task) => task,
            None => {
                return Poll::Ready(Err(Error::new(
                    buffer,
                    std::io::Error::new(
                        ErrorKind::Other,
                        format!("Task not found for token {}.", token),
                    ),
                )))
            }
        };

        // TODO: We should check the state of the system. Can only do this when it
        // is Idle
        task.operations.insert(
            overlapped.raw(),
            (
                overlapped,
                Operation::Write(buffer),
                OpState::Pending(ctx.waker().clone()),
            ),
        );
        Poll::Pending
    }

    // TODO: It looks like we are dropping the accept buffer. We should treat this similar to
    // pending_buffer_operation.
    pub(crate) fn pending_accept_operation(
        &mut self,
        ctx: &mut Context<'_>,
        token: usize,
        overlapped: Box<Overlapped>,
        stream: TcpStream,
        addrs: Box<AcceptAddrsBuf>,
    ) -> Poll<IoResult<()>> {
        let reactor = self.reactor.upgrade().ok_or(std::io::Error::new(
            ErrorKind::Other,
            "Event loop was shutdown",
        ))?;

        let mut slab = reactor.slab.borrow_mut();
        let task = slab.get_mut(token).ok_or(std::io::Error::new(
            ErrorKind::Other,
            format!("Task not found for token {}.", token),
        ))?;

        // TODO: We should check the state of the system. Can only do this when it
        // is Idle
        task.operations.insert(
            overlapped.raw(),
            (
                overlapped,
                Operation::Accept(stream, addrs),
                OpState::Pending(ctx.waker().clone()),
            ),
        );
        Poll::Pending
    }

    pub(crate) fn pending_connect_operation(
        &mut self,
        ctx: &mut Context<'_>,
        token: usize,
        overlapped: Box<Overlapped>,
        stream: TcpStream,
    ) -> Poll<IoResult<()>> {
        let reactor = self.reactor.upgrade().ok_or(std::io::Error::new(
            ErrorKind::Other,
            "Event loop was shutdown",
        ))?;

        let mut slab = reactor.slab.borrow_mut();
        let task = slab.get_mut(token).ok_or(std::io::Error::new(
            ErrorKind::Other,
            format!("Task not found for token {}.", token),
        ))?;

        // TODO: We should check the state of the system. Can only do this when it
        // is Idle
        task.operations.insert(
            overlapped.raw(),
            (
                overlapped,
                Operation::Connect(stream),
                OpState::Pending(ctx.waker().clone()),
            ),
        );
        Poll::Pending
    }

    pub(crate) fn timer(&self, ctx: &mut Context<'_>, at: Instant) -> IoResult<()> {
        self.reactor
            .upgrade()
            .ok_or(std::io::Error::new(
                ErrorKind::Other,
                "Event loop was shutdown",
            ))
            .map(|reactor| reactor.timer.timer(ctx, at))
    }
}
