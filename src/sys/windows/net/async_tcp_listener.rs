use crate::net::AsyncTcpStream;
use crate::sys::executor::Operation;
use crate::sys::executor::Registry;
use core::pin::Pin;
use core::task::Context;
use core::task::Poll;
use futures::ready;
use futures::Stream;
use miow::net::AcceptAddrsBuf;
use miow::net::TcpListenerExt;
use miow::Overlapped;
use socket2::Domain;
use socket2::Socket;
use socket2::Type;
use std::io::Error;
use std::io::Result as IoResult;
use std::net::SocketAddr;
use std::net::TcpListener;
use std::net::ToSocketAddrs;
use std::os::windows::io::AsRawSocket;
use winapi::um::minwinbase::OVERLAPPED;
use winapi::um::winnt::HANDLE;

// TODO: To save some space, make overlapped_raw and state mutually exclusive states
pub struct AsyncTcpListener {
    tcp_listener: TcpListener,
    registry: Registry,
    token: usize,
    overlapped_raw: *mut OVERLAPPED,
    state: Option<(Box<AcceptAddrsBuf>, Box<Overlapped>)>,
}

impl AsyncTcpListener {
    fn new(mut registry: Registry, tcp_listener: TcpListener) -> IoResult<AsyncTcpListener> {
        // TODO: Share this code
        {
            let flags = winapi::um::winbase::FILE_SKIP_SET_EVENT_ON_HANDLE
                | winapi::um::winbase::FILE_SKIP_COMPLETION_PORT_ON_SUCCESS;
            let r = unsafe {
                winapi::um::winbase::SetFileCompletionNotificationModes(
                    tcp_listener.as_raw_socket() as HANDLE,
                    flags,
                )
            };
            if r != winapi::shared::minwindef::TRUE {
                return Err(Error::last_os_error());
            }
        }

        let token = registry.add_socket(&tcp_listener)?;
        let overlapped = Box::new(Overlapped::zero());

        Ok(AsyncTcpListener {
            tcp_listener,
            registry,
            token,
            overlapped_raw: overlapped.raw(),
            state: Some((Box::new(AcceptAddrsBuf::new()), overlapped)),
        })
    }

    pub fn bind<T>(reactor: Registry, addrs: T) -> IoResult<AsyncTcpListener>
    where
        T: ToSocketAddrs,
    {
        let listener = TcpListener::bind(addrs)?;

        AsyncTcpListener::new(reactor, listener)
    }
}

impl Stream for AsyncTcpListener {
    type Item = IoResult<AsyncTcpStream>;

    fn poll_next(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let this = self.get_mut();
        if let Some((overlapped, operation)) =
            ready!(this
                .registry
                .poll_completed_operation(ctx, this.token, this.overlapped_raw))?
        {
            let (socket, addrs) = match operation {
                Operation::Accept(socket, addrs) => (socket, addrs),
                Operation::Read(_) | Operation::Write(_) | Operation::Connect(_) => unreachable!(),
            };

            let (transferred, _) = unsafe { this.tcp_listener.result(this.overlapped_raw)? };
            assert_eq!(
                transferred, 0,
                "Error with pending accept. Expected 0 for transferred size."
            );

            this.tcp_listener.accept_complete(&socket)?;

            this.state = Some((addrs, overlapped));

            Poll::Ready(Some(AsyncTcpStream::new(this.registry.clone(), socket)))
        } else if let Some((mut addrs, overlapped)) = this.state.take() {
            let domain = match this.tcp_listener.local_addr()? {
                SocketAddr::V4(..) => Domain::ipv4(),
                SocketAddr::V6(..) => Domain::ipv6(),
            };
            let socket = Socket::new(domain, Type::stream(), None)?.into_tcp_stream();

            if unsafe {
                this.tcp_listener
                    .accept_overlapped(&socket, &mut addrs, overlapped.raw())?
            } {
                // NOTE: If changed to complete synchronously, then make sure that those event
                // skip the IOCP. See FILE_SKIP_SET_EVENT_ON_HANDLE and
                // FILE_SKIP_COMPLETION_PORT_ON_SUCCESS.
                panic!("Accept should never complete synchronously: {}", this.token)
            } else {
                ready!(this
                    .registry
                    .pending_accept_operation(ctx, this.token, overlapped, socket, addrs))?;
                unreachable!()
            }
        } else {
            Poll::Pending
        }
    }
}
