use crate::buffer::Buf;
use crate::buffer::BufMut;
use crate::buffer::ReadView;
use crate::buffer::WriteView;
use crate::io::AsyncRead;
use crate::io::AsyncWrite;
use crate::io::Error;
use crate::sys::executor::Operation;
use crate::sys::executor::Registry;
use core::future::Future;
use core::mem::MaybeUninit;
use core::pin::Pin;
use futures::ready;
use miow::net::TcpStreamExt;
use miow::Overlapped;
use socket2::Domain;
use socket2::Socket;
use socket2::Type;
use std::io::ErrorKind;
use std::io::Result as IoResult;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::net::SocketAddr;
use std::net::SocketAddrV4;
use std::net::SocketAddrV6;
use std::net::TcpStream;
use std::net::ToSocketAddrs;
use std::os::windows::io::AsRawSocket;
use std::task::Context;
use std::task::Poll;
use winapi::um::minwinbase::OVERLAPPED;
use winapi::um::winnt::HANDLE;

// TODO: Reduce space by making *_overlapped and *_overlapped_raw mutually exclusive
pub struct AsyncTcpStream {
    tcp_stream: TcpStream,
    registry: Registry,
    token: usize,
    write_overlapped_raw: *mut OVERLAPPED,
    write_overlapped: Option<Box<Overlapped>>,
    read_overlapped_raw: *mut OVERLAPPED,
    read_overlapped: Option<Box<Overlapped>>,
}

impl AsyncTcpStream {
    pub(crate) fn new(mut registry: Registry, tcp_stream: TcpStream) -> IoResult<Self> {
        // TODO XXX: Share this code
        {
            let flags = winapi::um::winbase::FILE_SKIP_SET_EVENT_ON_HANDLE
                | winapi::um::winbase::FILE_SKIP_COMPLETION_PORT_ON_SUCCESS;
            let r = unsafe {
                winapi::um::winbase::SetFileCompletionNotificationModes(
                    tcp_stream.as_raw_socket() as HANDLE,
                    flags,
                )
            };
            if r != winapi::shared::minwindef::TRUE {
                return Err(std::io::Error::last_os_error());
            }
        }

        let token = registry.add_socket(&tcp_stream)?;
        let write_overlapped = Box::new(Overlapped::zero());
        let read_overlapped = Box::new(Overlapped::zero());

        Ok(AsyncTcpStream {
            tcp_stream,
            registry,
            token,
            write_overlapped_raw: write_overlapped.raw(),
            write_overlapped: Some(write_overlapped),
            read_overlapped_raw: read_overlapped.raw(),
            read_overlapped: Some(read_overlapped),
        })
    }

    pub fn connect<T>(registry: Registry, addrs: T) -> ConnectFuture<T>
    where
        T: ToSocketAddrs,
    {
        ConnectFuture::new(registry, addrs)
    }

    fn connect_completed(
        registry: Registry,
        tcp_stream: TcpStream,
        token: usize,
        write_overlapped: Box<Overlapped>,
    ) -> IoResult<AsyncTcpStream> {
        tcp_stream.connect_complete()?;

        let read_overlapped = Box::new(Overlapped::zero());

        Ok(AsyncTcpStream {
            tcp_stream,
            registry,
            token,
            write_overlapped_raw: write_overlapped.raw(),
            write_overlapped: Some(write_overlapped),
            read_overlapped_raw: read_overlapped.raw(),
            read_overlapped: Some(read_overlapped),
        })
    }
}

impl AsyncRead for AsyncTcpStream {
    fn poll_read_ready(
        &mut self,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<Option<(usize, WriteView)>, Error<WriteView>>> {
        match ready!(self.registry.poll_completed_operation(
            ctx,
            self.token,
            self.read_overlapped_raw
        )) {
            Ok(Some((overlapped, operation))) => {
                self.read_overlapped = Some(overlapped);

                let mut buffer = match operation {
                    Operation::Read(buffer) => buffer,
                    Operation::Write(_) | Operation::Accept(..) | Operation::Connect(_) => {
                        unreachable!()
                    }
                };

                match unsafe { self.tcp_stream.result(self.read_overlapped_raw) } {
                    Ok((size, _)) => {
                        // safety: read operation wrote `size` bytes to buffer
                        unsafe {
                            buffer.advance_write_index(size);
                        }
                        Poll::Ready(Ok(Some((size, buffer))))
                    }
                    Err(error) => Poll::Ready(Err(Error::new(buffer, error))),
                }
            }
            Ok(None) => Poll::Ready(Ok(None)),
            Err(_) => {
                // If the IOCP is gone just assume that we are ready. The error will get re-publish
                // when the future attempts to perform a poll_read.
                Poll::Ready(Ok(None))
            }
        }
    }

    fn poll_read(
        &mut self,
        ctx: &mut Context<'_>,
        mut buffer: WriteView,
    ) -> Poll<Result<(usize, WriteView), Error<WriteView>>> {
        if let Some(read_overlapped) = self.read_overlapped.take() {
            // TODO: The long term solution is to implement a version of
            // `self.tcp_stream.read_overlapped` that takes a slice of `MaybeUninit<u8>`.
            //
            // safety: calling `slide_get_mut` is okay because `self.stream.read` only writes to
            // the slice.
            match unsafe {
                self.tcp_stream.read_overlapped(
                    MaybeUninit::slice_get_mut(buffer.write_mut_slice()),
                    read_overlapped.raw(),
                )
            } {
                Ok(Some(size)) => {
                    // safety: read operation wrote `size` bytes to buffer
                    unsafe {
                        buffer.advance_write_index(size);
                    }

                    self.read_overlapped = Some(read_overlapped);

                    Poll::Ready(Ok((size, buffer)))
                }
                Ok(None) => {
                    ready!(self.registry.pending_read_operation(
                        ctx,
                        self.token,
                        read_overlapped,
                        buffer
                    ))?;
                    unreachable!()
                }
                Err(error) => Poll::Ready(Err(Error::new(buffer, error))),
            }
        } else {
            Poll::Ready(Err(Error::new(
                buffer,
                std::io::Error::new(
                    ErrorKind::Other,
                    "Error reading buffer, AsyncTcpStream is full",
                ),
            )))
        }
    }
}

impl AsyncWrite for AsyncTcpStream {
    fn poll_write_ready(
        &mut self,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<Option<(usize, ReadView)>, Error<ReadView>>> {
        match ready!(self.registry.poll_completed_operation(
            ctx,
            self.token,
            self.write_overlapped_raw
        )) {
            Ok(Some((overlapped, operation))) => {
                self.write_overlapped = Some(overlapped);

                let mut buffer = match operation {
                    Operation::Write(buffer) => buffer,
                    Operation::Read(_) | Operation::Accept(..) | Operation::Connect(_) => {
                        unreachable!()
                    }
                };

                match unsafe { self.tcp_stream.result(self.write_overlapped_raw) } {
                    Ok((size, _)) => {
                        buffer.advance_read_index(size);
                        Poll::Ready(Ok(Some((size, buffer))))
                    }
                    Err(error) => Poll::Ready(Err(Error::new(buffer, error))),
                }
            }
            Ok(None) => Poll::Ready(Ok(None)),
            Err(_) => {
                // If the IOCP is gone just assume that we are ready. The error will get re-publish
                // when the future attempts to perform a poll_write.
                Poll::Ready(Ok(None))
            }
        }
    }

    fn poll_write(
        &mut self,
        ctx: &mut Context<'_>,
        mut buffer: ReadView,
    ) -> Poll<Result<(usize, ReadView), Error<ReadView>>> {
        if let Some(write_overlapped) = self.write_overlapped.take() {
            match unsafe {
                self.tcp_stream
                    .write_overlapped(buffer.read_slice(), write_overlapped.raw())
            } {
                Ok(Some(size)) => {
                    buffer.advance_read_index(size);

                    self.write_overlapped = Some(write_overlapped);

                    Poll::Ready(Ok((size, buffer)))
                }
                Ok(None) => {
                    ready!(self.registry.pending_write_operation(
                        ctx,
                        self.token,
                        write_overlapped,
                        buffer,
                    ))?;
                    unreachable!()
                }
                Err(error) => Poll::Ready(Err(Error::new(buffer, error))),
            }
        } else {
            Poll::Ready(Err(Error::new(
                buffer,
                std::io::Error::new(
                    ErrorKind::Other,
                    "Error writing buffer, AsyncTcpStream is full",
                ),
            )))
        }
    }
}

impl Drop for AsyncTcpStream {
    fn drop(&mut self) {
        let _ = self.registry.remove_socket(self.token);
    }
}

// The correct state should be Either[(token, overlapped_raw), overlapped]
pub struct ConnectFuture<T> {
    addrs: T,
    registry: Registry,
    token: Option<usize>,
    overlapped_raw: *mut OVERLAPPED,
    overlapped: Option<Box<Overlapped>>,
}

impl<T> ConnectFuture<T> {
    fn new(registry: Registry, addrs: T) -> Self {
        let overlapped = Box::new(Overlapped::zero());

        ConnectFuture {
            addrs,
            registry,
            token: None,
            overlapped_raw: overlapped.raw(),
            overlapped: Some(overlapped),
        }
    }
}

impl<T> Future for ConnectFuture<T>
where
    T: ToSocketAddrs + Unpin,
{
    type Output = IoResult<AsyncTcpStream>;

    fn poll(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        if let Some(overlapped) = this.overlapped.take() {
            let addrs = this.addrs.to_socket_addrs()?;

            let mut last_err = None;
            for addr in addrs {
                let (domain, local_addr) = match addr {
                    SocketAddr::V4(..) => (
                        Domain::ipv4(),
                        SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), 0)),
                    ),
                    SocketAddr::V6(..) => (
                        Domain::ipv6(),
                        SocketAddr::V6(SocketAddrV6::new(
                            Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0),
                            0,
                            0,
                            0,
                        )),
                    ),
                };

                let socket = Socket::new(domain, Type::stream(), None)?;
                socket.bind(&local_addr.into())?;

                let tcp_stream = socket.into_tcp_stream();

                // TODO XXX: Share this code. Move this to the connect future?
                {
                    let flags = winapi::um::winbase::FILE_SKIP_SET_EVENT_ON_HANDLE
                        | winapi::um::winbase::FILE_SKIP_COMPLETION_PORT_ON_SUCCESS;
                    let r = unsafe {
                        winapi::um::winbase::SetFileCompletionNotificationModes(
                            tcp_stream.as_raw_socket() as HANDLE,
                            flags,
                        )
                    };
                    if r != winapi::shared::minwindef::TRUE {
                        Err(std::io::Error::last_os_error())?;
                    }
                }

                let token = this.registry.add_socket(&tcp_stream)?;

                match unsafe { tcp_stream.connect_overlapped(&addr, &[], overlapped.raw()) } {
                    Ok(Some(_)) => panic!("Connect should never complete synchronously: {}", token),
                    Ok(None) => {
                        this.token = Some(token);
                        ready!(this
                            .registry
                            .pending_connect_operation(ctx, token, overlapped, tcp_stream))?;
                        unreachable!()
                    }
                    Err(err) => {
                        this.registry.remove_socket(token)?;
                        last_err = Some(err)
                    }
                }
            }

            Poll::Ready(Err(last_err.unwrap_or_else(|| {
                std::io::Error::new(ErrorKind::InvalidInput, "could not resolve to any address")
            })))
        } else if let Some(token) = this.token {
            if let Some((overlapped, operation)) =
                ready!(this
                    .registry
                    .poll_completed_operation(ctx, token, this.overlapped_raw))?
            {
                let tcp_stream = match operation {
                    Operation::Connect(tcp_stream) => tcp_stream,
                    Operation::Accept(..) | Operation::Read(_) | Operation::Write(_) => {
                        unreachable!()
                    }
                };

                let (transferred, _) = unsafe { tcp_stream.result(this.overlapped_raw)? };
                assert_eq!(
                    transferred, 0,
                    "Error with pending connect. Expected 0 for transferred size."
                );

                Poll::Ready(AsyncTcpStream::connect_completed(
                    this.registry.clone(),
                    tcp_stream,
                    token,
                    overlapped,
                ))
            } else {
                panic!("Error when quering pending operation. Pending operation already completed")
            }
        } else {
            panic!("Error when polling already completed ConnectFuture")
        }
    }
}
