use core::future::Future;
use core::pin::Pin;
use core::task::Context;
use core::task::Poll;
use futures::ready;
use futures::stream::Fuse;
use futures::stream::Stream;
use futures::stream::StreamExt;

pub trait Sink<Item> {
    type Error;

    fn poll_ready(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>>;
    fn poll_next_item(
        self: Pin<&mut Self>,
        ctx: &mut Context<'_>,
        item: Item,
    ) -> Poll<Result<(), Self::Error>>;
    fn poll_complete(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>>;
}

impl<Si, Item> Sink<Item> for &mut Si
where
    Si: Sink<Item> + Unpin + ?Sized,
{
    type Error = Si::Error;

    fn poll_ready(
        mut self: Pin<&mut Self>,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<(), Self::Error>> {
        Pin::new(&mut **self).poll_ready(ctx)
    }

    fn poll_next_item(
        mut self: Pin<&mut Self>,
        ctx: &mut Context<'_>,
        item: Item,
    ) -> Poll<Result<(), Self::Error>> {
        Pin::new(&mut **self).poll_next_item(ctx, item)
    }

    fn poll_complete(
        mut self: Pin<&mut Self>,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<(), Self::Error>> {
        Pin::new(&mut **self).poll_complete(ctx)
    }
}

impl<T> Sink<T> for Vec<T>
where
    T: Unpin,
{
    type Error = ();

    fn poll_ready(self: Pin<&mut Self>, _ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn poll_next_item(
        self: Pin<&mut Self>,
        _ctx: &mut Context<'_>,
        item: T,
    ) -> Poll<Result<(), Self::Error>> {
        self.get_mut().push(item);
        Poll::Ready(Ok(()))
    }

    fn poll_complete(
        self: Pin<&mut Self>,
        _ctx: &mut Context<'_>,
    ) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }
}

impl<Si, Item> SinkExt<Item> for Si where Si: Sink<Item> + ?Sized {}

pub trait SinkExt<Item>: Sink<Item> {
    fn send(&mut self, item: Item) -> Send<'_, Self, Item>
    where
        Self: Unpin,
    {
        Send::new(self, item)
    }

    fn comsume<'a, St>(&'a mut self, stream: &'a mut St) -> Consume<'a, Self, St>
    where
        St: Stream<Item = Item> + Unpin + ?Sized,
        Self: Unpin,
    {
        Consume::new(self, stream)
    }
}

pub struct Send<'a, Si, Item>
where
    Si: Sink<Item> + Unpin + ?Sized,
{
    sink: &'a mut Si,
    item: Option<Item>,
}

impl<Si, Item> Unpin for Send<'_, Si, Item> where Si: Sink<Item> + Unpin + ?Sized {}

impl<'a, Si, Item> Send<'a, Si, Item>
where
    Si: Sink<Item> + Unpin + ?Sized,
{
    fn new(sink: &'a mut Si, item: Item) -> Self {
        Send {
            sink,
            item: Some(item),
        }
    }
}

impl<Si, Item> Future for Send<'_, Si, Item>
where
    Si: Sink<Item> + Unpin + ?Sized,
{
    type Output = Result<(), Si::Error>;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = &mut *self;
        let mut sink = Pin::new(&mut this.sink);

        if let Some(item) = this.item.take() {
            match sink.as_mut().poll_ready(ctx) {
                Poll::Ready(Ok(())) => ready!(sink.as_mut().poll_next_item(ctx, item))?,
                Poll::Ready(Err(e)) => return Poll::Ready(Err(e)),
                Poll::Pending => {
                    this.item = Some(item);
                    return Poll::Pending;
                }
            }
        }

        sink.poll_complete(ctx)
    }
}

pub struct Consume<'a, Si, St>
where
    Si: Sink<St::Item> + ?Sized,
    St: Stream + Unpin + ?Sized,
{
    sink: &'a mut Si,
    stream: Fuse<&'a mut St>,
}

impl<Si, St> Unpin for Consume<'_, Si, St>
where
    Si: Sink<St::Item> + Unpin + ?Sized,
    St: Stream + Unpin + ?Sized,
{
}

impl<'a, Si, St> Consume<'a, Si, St>
where
    Si: Sink<St::Item> + Unpin + ?Sized,
    St: Stream + Unpin + ?Sized,
{
    fn new(sink: &'a mut Si, stream: &'a mut St) -> Self {
        Consume {
            sink,
            stream: stream.fuse(),
        }
    }
}

impl<Si, St> Future for Consume<'_, Si, St>
where
    Si: Sink<St::Item> + Unpin + ?Sized,
    St: Stream + Unpin + ?Sized,
{
    type Output = Result<(), Si::Error>;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = &mut *self;

        loop {
            ready!(Pin::new(&mut this.sink).poll_ready(ctx))?;

            match ready!(this.stream.poll_next_unpin(ctx)) {
                Some(item) => ready!(Pin::new(&mut this.sink).poll_next_item(ctx, item))?,
                None => break Pin::new(&mut this.sink).poll_complete(ctx),
            }
        }
    }
}
