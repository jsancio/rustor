#![feature(
    arbitrary_self_types,
    maybe_uninit_slice,
    maybe_uninit_slice_assume_init,
    new_uninit
)]

#[cfg(unix)]
#[path = "sys/linux/mod.rs"]
mod sys;
#[cfg(windows)]
#[path = "sys/windows/mod.rs"]
mod sys;

pub mod buffer;
pub mod codec;
pub mod executor;
pub mod io;
pub mod net;
pub mod sink;
pub mod time;
