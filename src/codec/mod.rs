use crate::buffer::Buf;
use crate::buffer::BufMutExt;
use crate::buffer::Buffer;
use crate::io::copy;
use crate::io::AsyncRead;
use crate::io::AsyncReadExt;
use crate::io::AsyncWrite;
use crate::io::AsyncWriteExt;
use crate::io::Error;
use core::str::from_utf8;
use http::Request;
use http::Response;
use http::Version;
use httparse::Status;
use httparse::EMPTY_HEADER;
use std::io::ErrorKind;

mod http_body;
pub use http_body::HttpBody;

pub async fn encode_http_response_parts<W>(
    writer: &mut W,
    mut header_buffer: Buffer,
    parts: http::response::Parts,
) -> Result<Buffer, Error<Buffer>>
where
    W: AsyncWrite,
{
    // ===== write the header =====
    let bytes = format!("{:?} {}", parts.version, parts.status).into_bytes();
    header_buffer.put_slice(&bytes);
    for (key, value) in parts.headers.iter() {
        header_buffer.put_slice(b"\r\n");
        header_buffer.put_slice(key.as_str().as_bytes());
        header_buffer.put_slice(b": ");
        header_buffer.put_slice(value.as_bytes());
    }
    header_buffer.put_slice(b"\r\n\r\n");

    writer
        .write_all(header_buffer.read_view())
        .await
        .map(|read_view| read_view.buffer())
        .map_err(|error| {
            let (read_view, error) = error.into();
            Error::new(read_view.buffer(), error)
        })
}

pub async fn encode_http_response<R, W>(
    writer: &mut W,
    mut header_buffer: Buffer,
    response: Response<(Buffer, R)>,
) -> Result<(Buffer, Buffer), Error<(Buffer, Buffer)>>
where
    R: AsyncRead,
    W: AsyncWrite,
{
    let (parts, (mut body_buffer, mut body)) = response.into_parts();

    header_buffer = match encode_http_response_parts(writer, header_buffer, parts).await {
        Ok(buffer) => buffer,
        Err(error) => {
            let (buffer, error) = error.into();
            return Err(Error::new((buffer, body_buffer), error));
        }
    };

    // ===== write the body =====
    body_buffer = match copy(&mut body, body_buffer, writer).await {
        Ok((_, buffer)) => buffer,
        Err(error) => {
            let (buffer, error) = error.into();
            return Err(Error::new((header_buffer, buffer), error));
        }
    };

    Ok((header_buffer, body_buffer))
}

pub async fn decode_http_request<R>(
    reader: &mut R,
    mut buffer: Buffer,
) -> Result<Option<Request<(Buffer, HttpBody<'_, R>)>>, Error<Buffer>>
where
    R: AsyncRead,
{
    let mut first_time = true;
    loop {
        let (size, write_view) = reader.read(buffer.write_view()).await?;
        buffer = write_view.buffer();

        if size == 0 {
            // Peer closed connection
            if first_time {
                return Ok(None);
            } else {
                return Err(Error::new(buffer, ErrorKind::UnexpectedEof.into()));
            }
        }

        first_time = false;

        // TODO: header size should be configurable
        let mut headers = vec![EMPTY_HEADER; 16];
        let mut request_parser = httparse::Request::new(&mut headers);

        let header_size = match request_parser.parse(&mut buffer.read_slice()) {
            Err(_) => {
                return Err(Error::new(
                    buffer,
                    std::io::Error::new(ErrorKind::Other, "Some parsing error"),
                ));
            }
            Ok(Status::Partial) => continue, // Don't have enough bytes; continue reading
            Ok(Status::Complete(header_size)) => header_size,
        };

        let mut request_builder = Request::builder()
            .method(request_parser.method.unwrap())
            .uri(request_parser.path.unwrap())
            .version(if request_parser.version.unwrap() == b'1' {
                Version::HTTP_11
            } else {
                Version::HTTP_10
            });

        let mut content_length = 0;
        for header in headers {
            if header != EMPTY_HEADER {
                request_builder = request_builder.header(header.name, header.value);

                if header.name.trim().eq_ignore_ascii_case("content-length") {
                    content_length = match from_utf8(header.value) {
                        Ok(value) => match value.parse::<usize>() {
                            Ok(content_length) => content_length,
                            Err(_) => {
                                return Err(Error::new(
                                    buffer,
                                    std::io::Error::new(
                                        ErrorKind::Other,
                                        "Error while parsing the content length header",
                                    ),
                                ))
                            }
                        },
                        Err(_) => {
                            return Err(Error::new(
                                buffer,
                                std::io::Error::new(
                                    ErrorKind::Other,
                                    "Error while decoding content length as an UTF8",
                                ),
                            ))
                        }
                    };
                }
            }
        }

        buffer.advance_read_index(header_size);

        break match request_builder.body(()) {
            Ok(request) => Ok(Some(request.map(move |()| {
                let body = HttpBody::new(reader, content_length, buffer.read_slice().len());
                (buffer, body)
            }))),
            Err(_) => Err(Error::new(
                buffer,
                std::io::Error::new(ErrorKind::Other, "Some HTTP request error"),
            )),
        };
    }
}
