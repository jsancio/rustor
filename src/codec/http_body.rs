use crate::buffer::BufMut;
use crate::buffer::WriteView;
use crate::io::AsyncRead;
use crate::io::Error;
use core::cmp::min;
use core::task::Context;
use core::task::Poll;
use futures::ready;
use std::io::ErrorKind;

#[derive(Debug)]
pub struct HttpBody<'a, R> {
    reader: &'a mut R,
    content_length: usize,
    bytes_read: usize,
}

impl<'a, R> HttpBody<'a, R> {
    pub fn new(reader: &'a mut R, content_length: usize, bytes_read: usize) -> Self {
        HttpBody {
            reader,
            content_length,
            bytes_read,
        }
    }
}

impl<'a, R> AsyncRead for HttpBody<'a, R>
where
    R: AsyncRead,
{
    fn poll_read_ready(
        &mut self,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<Option<(usize, WriteView)>, Error<WriteView>>> {
        match ready!(self.reader.poll_read_ready(ctx))? {
            Some((size, buffer)) => {
                self.bytes_read += size;
                Poll::Ready(Ok(Some((size, buffer))))
            }
            None => Poll::Ready(Ok(None)),
        }
    }

    fn poll_read(
        &mut self,
        ctx: &mut Context<'_>,
        buffer: WriteView,
    ) -> Poll<Result<(usize, WriteView), Error<WriteView>>> {
        if self.content_length > self.bytes_read {
            let read_bytes = min(
                self.content_length - self.bytes_read,
                buffer.write_slice().len(),
            );
            let (size, buffer) = ready!(self
                .reader
                .poll_read(ctx, buffer.sized_write_view(read_bytes)))?;

            self.bytes_read += size;

            if size == 0 {
                // Peer closed connection
                Poll::Ready(Err(Error::new(buffer, ErrorKind::UnexpectedEof.into())))
            } else {
                Poll::Ready(Ok((size, buffer)))
            }
        } else {
            Poll::Ready(Ok((0, buffer)))
        }
    }
}
