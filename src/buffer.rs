use core::mem::MaybeUninit;
use core::ops::Deref;
use core::ops::DerefMut;
use core::ptr::copy_nonoverlapping;

/*
 * buffer:
 * +------------+-----------------+--------------------+
 * | used bytes | readable bytes  | writable bytes     |
 * |            | [u8]            | [MaybeUninit<u8>]  |
 * |            | read_slice()    | write_mut_slice()  |
 * +------------+-----------------+--------------------|
 * |            |                 |                    |
 * 0    <=      read_index        wite_index           capacity()
 */

#[derive(Debug)]
pub struct Buffer {
    buffer: Box<[MaybeUninit<u8>]>,
    write_index: usize,
    read_index: usize,
}

impl Buffer {
    pub fn with_capacity(capacity: usize) -> Self {
        Buffer {
            buffer: Box::new_uninit_slice(capacity),
            write_index: 0,
            read_index: 0,
        }
    }

    pub fn capacity(&self) -> usize {
        self.buffer.len()
    }

    pub fn clear(&mut self) {
        self.write_index = 0;
        self.read_index = 0;
    }

    fn read_mut_slice(&mut self) -> &mut [u8] {
        unsafe { MaybeUninit::slice_get_mut(&mut self.buffer[self.read_index..self.write_index]) }
    }

    pub fn discard_read(&mut self) {
        if self.read_index != 0 {
            self.buffer
                .copy_within(self.read_index..self.write_index, 0);
            self.write_index -= self.read_index;
            self.read_index = 0;
        }
    }

    pub fn sized_read_view(self, len: usize) -> ReadView {
        assert!(len <= self.read_slice().len());

        ReadView { buffer: self, len }
    }

    pub fn read_view(self) -> ReadView {
        let len = self.read_slice().len();
        self.sized_read_view(len)
    }
}

impl Buf for Buffer {
    fn advance_read_index(&mut self, size: usize) {
        assert!(self.read_index + size <= self.write_index);
        self.read_index += size;
    }

    fn read_slice(&self) -> &[u8] {
        // safety: `write_index` is only increased when calling `advance_write_index` which is
        // unsafe.
        unsafe { MaybeUninit::slice_get_ref(&self.buffer[self.read_index..self.write_index]) }
    }
}

impl BufMut for Buffer {
    unsafe fn advance_write_index(&mut self, size: usize) {
        assert!(self.write_index + size <= self.buffer.len());
        self.write_index += size;
    }

    fn write_mut_slice(&mut self) -> &mut [MaybeUninit<u8>] {
        &mut self.buffer[self.write_index..]
    }

    fn write_slice(&self) -> &[MaybeUninit<u8>] {
        &self.buffer[self.write_index..]
    }

    fn sized_write_view(self, len: usize) -> WriteView {
        assert!(len <= self.write_slice().len());

        WriteView { buffer: self, len }
    }
}

impl From<ReadView> for Buffer {
    fn from(view: ReadView) -> Self {
        view.buffer()
    }
}

impl From<WriteView> for Buffer {
    fn from(view: WriteView) -> Self {
        view.buffer()
    }
}

pub struct ReadView {
    buffer: Buffer,
    len: usize,
}

impl ReadView {
    pub fn buffer(self) -> Buffer {
        self.buffer
    }
}

impl Deref for ReadView {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        &self.buffer.read_slice()[..self.len]
    }
}

impl DerefMut for ReadView {
    fn deref_mut(&mut self) -> &mut [u8] {
        &mut self.buffer.read_mut_slice()[..self.len]
    }
}

impl Buf for ReadView {
    fn advance_read_index(&mut self, size: usize) {
        assert!(size <= self.len);
        self.buffer.advance_read_index(size);
        self.len -= size;
    }

    fn read_slice(&self) -> &[u8] {
        self
    }
}

pub struct WriteView {
    buffer: Buffer,
    len: usize,
}

impl WriteView {
    pub fn buffer(self) -> Buffer {
        self.buffer
    }
}

impl Deref for WriteView {
    type Target = [MaybeUninit<u8>];

    fn deref(&self) -> &Self::Target {
        &self.buffer.write_slice()[..self.len]
    }
}

impl DerefMut for WriteView {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.buffer.write_mut_slice()[..self.len]
    }
}

impl BufMut for WriteView {
    unsafe fn advance_write_index(&mut self, size: usize) {
        assert!(size <= self.len);
        self.buffer.advance_write_index(size);
        self.len -= size;
    }

    fn write_mut_slice(&mut self) -> &mut [MaybeUninit<u8>] {
        self
    }

    fn write_slice(&self) -> &[MaybeUninit<u8>] {
        self
    }

    fn sized_write_view(self, len: usize) -> WriteView {
        assert!(len <= self.len);

        WriteView {
            buffer: self.buffer,
            len,
        }
    }
}

pub trait Buf {
    fn advance_read_index(&mut self, size: usize);
    fn read_slice(&self) -> &[u8];
}

pub trait BufExt: Buf {
    fn is_readable(&self) -> bool {
        self.read_slice().len() > 0
    }

    fn get_slice(&mut self, dst: &mut [u8]) {
        assert!(dst.len() <= self.read_slice().len());

        dst.copy_from_slice(&self.read_slice()[..dst.len()]);
        self.advance_read_index(dst.len());
    }
}

impl<B> BufExt for B where B: Buf {}

pub trait BufMut {
    // TODO: Talk about the safe requirement. For example, safety assumes that size bytes to the
    // ahead of write_index are initialized.
    unsafe fn advance_write_index(&mut self, size: usize);
    fn write_mut_slice(&mut self) -> &mut [MaybeUninit<u8>];
    fn write_slice(&self) -> &[MaybeUninit<u8>];
    fn sized_write_view(self, len: usize) -> WriteView;
}

pub trait BufMutExt: BufMut {
    fn is_writable(&self) -> bool {
        self.write_slice().len() > 0
    }

    fn put_slice(&mut self, src: &[u8]) {
        let dst = self.write_mut_slice();
        assert!(src.len() <= dst.len());

        // safety:
        // 1. src is valid for read for `src.len()`
        // 2. dst is valid for write for `src.len()`, since `src.len()` is <= `dst.len()`
        // 3. `src` and `dst` do not overlap because slice is a `&mut []` and buffer is a `&[]`
        unsafe {
            copy_nonoverlapping(src.as_ptr(), MaybeUninit::first_ptr_mut(dst), src.len());
            self.advance_write_index(src.len());
        }
    }

    fn write_view(self) -> WriteView
    where
        Self: Sized,
    {
        let len = self.write_slice().len();
        self.sized_write_view(len)
    }
}

impl<B> BufMutExt for B where B: BufMut {}
