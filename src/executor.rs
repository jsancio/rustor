use crate::sys::executor::Reactor;
pub use crate::sys::executor::Registry;
use core::cell::RefCell;
use core::sync::atomic::AtomicBool;
use core::sync::atomic::Ordering;
use core::task::Context;
use core::task::Poll;
use core::task::Waker;
use core::time::Duration;
use futures::executor::enter;
use futures::future::LocalFutureObj;
use futures::stream::FuturesUnordered;
use futures::stream::StreamExt;
use futures::task::waker;
use futures::task::ArcWake;
use futures::task::LocalSpawn;
use futures::task::SpawnError;
use std::collections::BTreeMap;
use std::io::Result as IoResult;
use std::rc::Rc;
use std::rc::Weak;
use std::sync::Arc;
use std::time::Instant;

type Incoming = RefCell<Vec<LocalFutureObj<'static, ()>>>;

pub struct EventLoop {
    reactor: Rc<Reactor>,
    pool: FuturesUnordered<LocalFutureObj<'static, ()>>,
    incoming: Rc<Incoming>,
}

impl EventLoop {
    pub fn new() -> IoResult<EventLoop> {
        Ok(EventLoop {
            reactor: Rc::new(Reactor::new()?),
            pool: FuturesUnordered::new(),
            incoming: Default::default(),
        })
    }

    pub fn spawner(&self) -> LocalSpawner {
        LocalSpawner {
            incoming: Rc::downgrade(&self.incoming),
        }
    }

    pub fn registry(&self) -> Registry {
        Registry::new(Rc::downgrade(&self.reactor))
    }

    fn poll_pool(&mut self, waker: &mut Context<'_>) -> Poll<()> {
        loop {
            for task in self.incoming.borrow_mut().drain(..) {
                self.pool.push(task)
            }

            let ret = self.pool.poll_next_unpin(waker);

            // TODO: Revisit this. If incoming is not empty we would starve for task that are
            // waiting on IO.
            if self.incoming.borrow().is_empty() {
                match ret {
                    Poll::Pending => break Poll::Pending,
                    Poll::Ready(None) => break Poll::Ready(()),
                    Poll::Ready(Some(())) => {}
                }
            }
        }
    }

    fn wait_on_events(&mut self, check_only: bool) -> IoResult<()> {
        // TODO: Fix this when we generalized timer support
        let (woken, next_timer) = self.reactor.wake_on_timer();

        let delay = if check_only || woken {
            // There are pending tasks, check io events but don't block.
            Some(Duration::from_millis(0))
        } else {
            next_timer.and_then(|at| {
                let now = Instant::now();
                if at >= now {
                    // There aren't any timers that triggered, check io events and block at most
                    // until the next timer.
                    Some(at - now)
                } else {
                    // There are timers that triggered, check io events but don't block.
                    Some(Duration::from_millis(0))
                }
            })
        };

        self.reactor.wake_on_io(delay)
    }

    pub fn run(&mut self) -> IoResult<()> {
        let _enter = enter().expect("Cannot execute 'EventLoop' from within another executor");

        let task_ready = Arc::new(TaskReady::new());
        let waker = waker(task_ready.clone());
        let mut context = Context::from_waker(&waker);
        loop {
            let ret = self.poll_pool(&mut context);

            if ret == Poll::Ready(()) {
                // There are no task; We must be done
                break Ok(());
            }

            self.wait_on_events(task_ready.clear())?;
        }
    }
}

struct TaskReady(AtomicBool);

impl TaskReady {
    fn new() -> Self {
        TaskReady(AtomicBool::new(false))
    }

    fn clear(&self) -> bool {
        self.0.swap(false, Ordering::Relaxed)
    }
}

impl ArcWake for TaskReady {
    fn wake_by_ref(arc_self: &Arc<Self>) {
        arc_self.0.store(true, Ordering::Relaxed);
    }
}

pub struct LocalSpawner {
    incoming: Weak<Incoming>,
}

impl LocalSpawn for LocalSpawner {
    fn spawn_local_obj(&self, future: LocalFutureObj<'static, ()>) -> Result<(), SpawnError> {
        if let Some(incoming) = self.incoming.upgrade() {
            incoming.borrow_mut().push(future);
            Ok(())
        } else {
            Err(SpawnError::shutdown())
        }
    }

    fn status_local(&self) -> Result<(), SpawnError> {
        if self.incoming.upgrade().is_some() {
            Ok(())
        } else {
            Err(SpawnError::shutdown())
        }
    }
}

pub(crate) struct Timer {
    timer_wakers: RefCell<BTreeMap<Instant, Vec<Waker>>>,
}

impl Timer {
    pub(crate) fn new() -> Self {
        Timer {
            timer_wakers: Default::default(),
        }
    }

    pub(crate) fn timer(&self, ctx: &mut Context<'_>, at: Instant) {
        let mut timer_wakers = self.timer_wakers.borrow_mut();
        let wakers = timer_wakers.entry(at).or_default();
        wakers.push(ctx.waker().clone());
    }

    /* Wake up all of the futures waiting on triggered timer events. For the first element in the
     * returned pair, it is true if a task was woken up; false otherwise. For the second element in
     * the return pair, it is the earliest pending timer event; None otherwise.
     */
    pub(crate) fn wake_on_timer(&self) -> (bool, Option<Instant>) {
        let mut woken = false;
        loop {
            let instant = self.timer_wakers.borrow().keys().next().cloned();
            match instant {
                Some(at) => {
                    let now = Instant::now();
                    if now >= at {
                        let wakers = self
                            .timer_wakers
                            .borrow_mut()
                            .remove(&at)
                            .expect("Inconsistent state. Unable to find expected key");

                        for waker in wakers {
                            woken = true;
                            waker.wake();
                        }
                    } else {
                        break (woken, Some(at));
                    }
                }
                None => break (woken, None),
            }
        }
    }
}
