use crate::buffer::Buffer;
use crate::buffer::ReadView;
use crate::buffer::WriteView;

#[derive(Debug)]
pub struct Error<T> {
    state: T,
    error: std::io::Error,
}

impl<T> Error<T> {
    pub fn new(state: T, error: std::io::Error) -> Self {
        Error { state, error }
    }

    pub fn drop_state(self) -> std::io::Error {
        self.error
    }

    pub fn drop_error(self) -> T {
        self.state
    }

    pub fn error(&self) -> &std::io::Error {
        &self.error
    }
}

impl<T> From<Error<T>> for std::io::Error {
    fn from(error: Error<T>) -> Self {
        error.drop_state()
    }
}

impl<T> From<Error<T>> for (T, std::io::Error) {
    fn from(error: Error<T>) -> Self {
        (error.state, error.error)
    }
}

impl From<Error<ReadView>> for Error<Buffer> {
    fn from(error: Error<ReadView>) -> Self {
        Error::new(error.state.into(), error.error)
    }
}

impl From<Error<WriteView>> for Error<Buffer> {
    fn from(error: Error<WriteView>) -> Self {
        Error::new(error.state.into(), error.error)
    }
}
