use crate::buffer::ReadView;
use crate::buffer::WriteView;
use crate::io::AsyncRead;
use crate::io::AsyncWrite;
use crate::io::Error;
use core::cell::RefCell;
use core::task::Context;
use core::task::Poll;
use std::rc::Rc;

pub struct ReadHalf<R> {
    reader: Rc<RefCell<R>>,
}

impl<R> AsyncRead for ReadHalf<R>
where
    R: AsyncRead,
{
    fn poll_read_ready(
        &mut self,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<Option<(usize, WriteView)>, Error<WriteView>>> {
        self.reader.borrow_mut().poll_read_ready(ctx)
    }

    fn poll_read(
        &mut self,
        ctx: &mut Context<'_>,
        buffer: WriteView,
    ) -> Poll<Result<(usize, WriteView), Error<WriteView>>> {
        self.reader.borrow_mut().poll_read(ctx, buffer)
    }
}

pub struct WriteHalf<W> {
    writer: Rc<RefCell<W>>,
}

impl<W> AsyncWrite for WriteHalf<W>
where
    W: AsyncWrite,
{
    fn poll_write_ready(
        &mut self,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<Option<(usize, ReadView)>, Error<ReadView>>> {
        self.writer.borrow_mut().poll_write_ready(ctx)
    }

    fn poll_write(
        &mut self,
        ctx: &mut Context<'_>,
        buffer: ReadView,
    ) -> Poll<Result<(usize, ReadView), Error<ReadView>>> {
        self.writer.borrow_mut().poll_write(ctx, buffer)
    }
}

pub fn split<T>(stream: T) -> (ReadHalf<T>, WriteHalf<T>)
where
    T: AsyncRead + AsyncWrite,
{
    let counter = Rc::new(RefCell::new(stream));
    (
        ReadHalf {
            reader: counter.clone(),
        },
        WriteHalf { writer: counter },
    )
}
