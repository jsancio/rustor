use crate::buffer::BufMutExt;
use crate::buffer::WriteView;
use crate::io::AsyncRead;
use crate::io::Error;
use core::future::Future;
use core::pin::Pin;
use core::task::Context;
use core::task::Poll;
use futures::ready;

pub struct Read<'a, R> {
    reader: &'a mut R,
    buffer: Option<WriteView>,
}

impl<'a, R> Read<'a, R> {
    pub fn new(reader: &'a mut R, buffer: WriteView) -> Self {
        Read {
            reader,
            buffer: Some(buffer),
        }
    }
}

impl<'a, R> Future for Read<'a, R>
where
    R: AsyncRead,
{
    type Output = Result<(usize, WriteView), Error<WriteView>>;

    fn poll(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        if let Some((size, buffer)) = ready!(this.reader.poll_read_ready(ctx))? {
            if this.buffer.is_some() {
                panic!("Illegal state. Reader already had a pending read.");
            }
            Poll::Ready(Ok((size, buffer)))
        } else if let Some(buffer) = this.buffer.take() {
            if buffer.is_writable() {
                this.reader.poll_read(ctx, buffer)
            } else {
                Poll::Ready(Ok((0, buffer)))
            }
        } else {
            panic!("Illegal state. Read performed on an already completed future.")
        }
    }
}
