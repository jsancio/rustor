use crate::buffer::BufExt;
use crate::buffer::ReadView;
use crate::io::AsyncWrite;
use crate::io::Error;
use core::future::Future;
use core::pin::Pin;
use core::task::Context;
use core::task::Poll;
use futures::ready;

pub struct WriteAll<'a, W> {
    writer: &'a mut W,
    buffer: Option<ReadView>,
}

impl<'a, W> WriteAll<'a, W> {
    pub fn new(writer: &'a mut W, buffer: ReadView) -> Self {
        WriteAll {
            writer,
            buffer: Some(buffer),
        }
    }
}

impl<'a, W> Future for WriteAll<'a, W>
where
    W: AsyncWrite,
{
    type Output = Result<ReadView, Error<ReadView>>;

    fn poll(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        loop {
            let buffer = if let Some((size, buffer)) = ready!(this.writer.poll_write_ready(ctx))? {
                if this.buffer.is_some() {
                    panic!("Error writing all; poll_write_ready returned an unexpected buffer");
                } else if size == 0 {
                    // TODO: Need to investigate how to handle this case. For now
                    // let's just panic!
                    panic!("Got a size of zero when writing all");
                } else {
                    buffer
                }
            } else {
                match this.buffer.take() {
                    Some(buffer) => buffer,
                    None => panic!(
                        "Error writing all; poll_write_ready didn't return an expected buffer"
                    ),
                }
            };

            if buffer.is_readable() {
                let (size, buffer) = ready!(this.writer.poll_write(ctx, buffer))?;
                if size == 0 {
                    // TODO: Need to investigate how to handle this case. For now
                    // let's just panic!
                    panic!("Got a size of zero when writing all");
                }
                this.buffer = Some(buffer);
            } else {
                break Poll::Ready(Ok(buffer));
            }
        }
    }
}
