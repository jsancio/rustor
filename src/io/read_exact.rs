use crate::buffer::BufMutExt;
use crate::buffer::WriteView;
use crate::io::AsyncRead;
use crate::io::Error;
use core::future::Future;
use core::pin::Pin;
use core::task::Context;
use core::task::Poll;
use futures::ready;
use std::io::ErrorKind;

pub struct ReadExact<'a, R> {
    reader: &'a mut R,
    buffer: Option<WriteView>,
}

impl<'a, R> ReadExact<'a, R> {
    pub fn new(reader: &'a mut R, buffer: WriteView) -> Self {
        ReadExact {
            reader,
            buffer: Some(buffer),
        }
    }
}

impl<'a, R> Future for ReadExact<'a, R>
where
    R: AsyncRead,
{
    type Output = Result<WriteView, Error<WriteView>>;

    fn poll(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        loop {
            let buffer = match (
                ready!(this.reader.poll_read_ready(ctx))?,
                this.buffer.take(),
            ) {
                (Some((size, buffer)), None) => {
                    if size == 0 {
                        break Poll::Ready(Err(Error::new(
                            buffer,
                            ErrorKind::UnexpectedEof.into(),
                        )));
                    } else {
                        buffer
                    }
                }
                (None, Some(buffer)) => buffer,
                (Some(_), Some(_)) => panic!("Read returned an unexpected buffer"),
                (None, None) => panic!("Read didn't return an expected buffer"),
            };

            if buffer.is_writable() {
                let (size, buffer) = ready!(this.reader.poll_read(ctx, buffer))?;
                // TODO: looks like code duplication from above
                if size == 0 {
                    break Poll::Ready(Err(Error::new(buffer, ErrorKind::UnexpectedEof.into())));
                } else {
                    this.buffer = Some(buffer);
                }
            } else {
                break Poll::Ready(Ok(buffer));
            }
        }
    }
}
