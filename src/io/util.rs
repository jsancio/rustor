use crate::buffer::Buf;
use crate::buffer::BufMutExt;
use crate::buffer::Buffer;
use crate::io::AsyncRead;
use crate::io::AsyncReadExt;
use crate::io::AsyncWrite;
use crate::io::AsyncWriteExt;
use crate::io::Error;

pub async fn copy<R, W>(
    reader: &mut R,
    mut buffer: Buffer,
    writer: &mut W,
) -> Result<(usize, Buffer), Error<Buffer>>
where
    R: AsyncRead,
    W: AsyncWrite,
{
    let mut written = buffer.read_slice().len();

    buffer = match writer.write_all(buffer.read_view()).await {
        Ok(read_view) => read_view.buffer(),
        Err(error) => {
            let (read_view, error) = error.into();
            return Err(Error::new(read_view.buffer(), error));
        }
    };

    loop {
        buffer.clear();
        let (size, write_view) = match reader.read(buffer.write_view()).await {
            Ok(result) => result,
            Err(error) => {
                let (write_view, error) = error.into();
                return Err(Error::new(write_view.buffer(), error));
            }
        };
        buffer = write_view.buffer();

        if size == 0 {
            break Ok((written, buffer));
        }

        written += buffer.read_slice().len();
        buffer = match writer.write_all(buffer.read_view()).await {
            Ok(read_view) => read_view.buffer(),
            Err(error) => {
                let (read_view, error) = error.into();
                return Err(Error::new(read_view.buffer(), error));
            }
        };
    }
}
