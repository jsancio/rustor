use crate::buffer::ReadView;
use crate::buffer::WriteView;
use core::task::Context;
use core::task::Poll;

mod util;
pub use util::copy;

mod error;
pub use error::Error;

mod read;
pub use read::Read;

mod read_exact;
pub use read_exact::ReadExact;

mod split;
pub use split::ReadHalf;
pub use split::WriteHalf;

mod write_all;
pub use write_all::WriteAll;

pub trait AsyncWrite {
    fn poll_write_ready(
        &mut self,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<Option<(usize, ReadView)>, Error<ReadView>>>;

    fn poll_write(
        &mut self,
        ctx: &mut Context<'_>,
        buffer: ReadView,
    ) -> Poll<Result<(usize, ReadView), Error<ReadView>>>;
}

pub trait AsyncRead {
    fn poll_read_ready(
        &mut self,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<Option<(usize, WriteView)>, Error<WriteView>>>;
    fn poll_read(
        &mut self,
        ctx: &mut Context<'_>,
        buffer: WriteView,
    ) -> Poll<Result<(usize, WriteView), Error<WriteView>>>;
}

pub trait AsyncWriteExt: AsyncWrite {
    fn write_all(&mut self, buffer: ReadView) -> WriteAll<'_, Self>
    where
        Self: Sized,
    {
        WriteAll::new(self, buffer)
    }
}

impl<W> AsyncWriteExt for W where W: AsyncWrite {}

pub trait AsyncReadExt: AsyncRead {
    fn read_exact(&mut self, buffer: WriteView) -> ReadExact<'_, Self>
    where
        Self: Sized,
    {
        ReadExact::new(self, buffer)
    }

    fn read(&mut self, buffer: WriteView) -> Read<'_, Self>
    where
        Self: Sized,
    {
        Read::new(self, buffer)
    }

    fn split(self) -> (ReadHalf<Self>, WriteHalf<Self>)
    where
        Self: AsyncWrite + Sized,
    {
        split::split(self)
    }
}

impl<R> AsyncReadExt for R where R: AsyncRead {}
