# Examples
## HTTP
Run basic echo HTTP server.

```
cargo +nightly run --release --example http
```

Make HTTP client requests.

```
curl -v --data 'hello world' http://localhost:8888

```

or send a file

```
curl -v --data-binary @path/to/file http://localhost:8888 > /tmp/echo.file
```

## RPC
Run basic RPC example.

```
cargo +nightly run --relase --example rpc
```

## Ping Pong
Run exampe ping pong server.

```
cargo +nightly run --release --example ping_pong
```

Run redis' benchmack.

```
redis-benchmark -t ping_inline -n 1000000 -P 1 -p 8888
```
