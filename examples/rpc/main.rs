#![feature(arbitrary_self_types)]

mod io;
mod rpc;
mod sink;

use core::fmt::Debug;
use core::future::Future;
use failure::Error;
use failure::Fail;
use futures::future::TryFuture;
use futures::future::TryFutureExt;
use futures::sink::SinkExt;
use futures::stream::unfold;
use futures::stream::TryStreamExt;
use futures::task::LocalSpawn;
use futures::task::SpawnError;
use io::recv_request;
use io::recv_response;
use io::send_request;
use io::send_response;
use io::Request;
use io::Response;
use pin_utils::pin_mut;
use rand;
use rpc::rpc;
use rpc::send;
use rustor::executor::EventLoop;
use rustor::executor::Registry;
use rustor::io::AsyncReadExt;
use rustor::net::AsyncTcpListener;
use rustor::net::AsyncTcpStream;
use rustor::time::Delay;
use sink::try_fold_sink;
use std::io::ErrorKind;
use std::time::Duration;

fn main() -> Result<(), Error> {
    let mut event_loop = EventLoop::new()?;

    // TODO XXX: Change this back to 10 when we have removed all errors in Windows and Linux.
    let number_of_clients = 505;

    event_loop
        .spawner()
        .spawn_local_obj(From::from(Box::new(handle_error(listen(
            number_of_clients,
            event_loop.registry(),
            event_loop.spawner(),
        )))))
        .map_err(RpcError::from)?;

    for _ in 0..number_of_clients {
        event_loop
            .spawner()
            .spawn_local_obj(From::from(Box::new(handle_error(client(
                event_loop.registry(),
                event_loop.spawner(),
            )))))
            .map_err(RpcError::from)?;
    }

    Ok(event_loop.run()?)
}

#[derive(Debug, Fail)]
enum RpcError {
    #[fail(display = "spawn error: shutdown")]
    Spawn,
}

impl From<SpawnError> for RpcError {
    fn from(_error: SpawnError) -> RpcError {
        RpcError::Spawn
    }
}

fn handle_error<Fut, E>(fut: Fut) -> impl Future<Output = ()>
where
    E: Debug,
    Fut: TryFuture<Ok = (), Error = E>,
{
    fut.unwrap_or_else(|err| {
        eprintln!("Error: {:?}", err);
        ()
    })
}

async fn listen<T: LocalSpawn>(
    mut number_of_clients: u16,
    reactor: Registry,
    spawner: T,
) -> Result<(), Error> {
    let mut listener = AsyncTcpListener::bind(reactor.clone(), "127.0.0.1:33333")?;

    Delay::new(reactor, Duration::from_secs(1)).await?;

    while number_of_clients > 0 {
        if let Some(stream) = listener.try_next().await? {
            spawner
                .spawn_local_obj(From::from(Box::new(handle_error(process(stream)))))
                .map_err(RpcError::from)?;
        } else {
            eprintln!("Error: listener stream ended unexpectively");
            break;
        }

        number_of_clients -= 1;
    }

    Ok(())
}

async fn process(stream: AsyncTcpStream) -> Result<(), Error> {
    let (read, write) = stream.split();
    let requests = unfold(read, |state| recv_request(state));
    let responses = try_fold_sink(write, send_response);
    pin_mut!(requests);
    pin_mut!(responses);

    while let Some((id, request)) = requests.try_next().await? {
        responses
            .send((id, Response::new(request.integer, request.float)))
            .await?;
    }

    Ok(())
}

async fn client<T>(reactor: Registry, spawner: T) -> Result<(), Error>
where
    T: LocalSpawn,
{
    let (read, write) = loop {
        // TODO: Implement future retry in crate
        match AsyncTcpStream::connect(reactor.clone(), "127.0.0.1:33333").await {
            Ok(tcp_stream) => break tcp_stream.split(),
            Err(ref err) if err.kind() == ErrorKind::ConnectionRefused => {}
            Err(err) => Err(err)?,
        }
    };

    let stream = unfold(read, recv_response);
    let sink = try_fold_sink(write, send_request);

    let (mut client, process) = rpc(stream, sink);
    spawner
        .spawn_local_obj(From::from(Box::pin(handle_error(process))))
        .map_err(RpcError::from)?;

    let content: Vec<(u64, f64)> = {
        let iteration: u8 = 255; // TODO: make this random again: rand::random();

        vec![(rand::random(), rand::random()); iteration as usize]
    };

    for (integer, float) in content {
        let response = send(&mut client, Request::new(integer, float)).await?;
        assert_eq!(
            (response.integer, response.float),
            (integer, float),
            "response didn't match requested value"
        )
    }

    Ok(())
}
