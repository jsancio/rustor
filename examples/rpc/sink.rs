use {
    core::pin::Pin,
    failure::Fail,
    futures::{future::TryFuture, sink::Sink},
    pin_utils::{unsafe_pinned, unsafe_unpinned},
    std::{
        marker::PhantomData,
        task::{Context, Poll},
    },
};

pub fn try_fold_sink<State, Item, Error, F, Fut>(
    write: State,
    func: F,
) -> TryFoldSink<State, Item, Error, F, Fut>
where
    Error: Fail,
    F: Fn(State, Item) -> Fut,
    Fut: TryFuture<Ok = State, Error = Error>,
{
    TryFoldSink {
        func,
        fut: None,
        write: Some(write),
        item_type: PhantomData,
    }
}

pub struct TryFoldSink<State, Item, Error, F, Fut>
where
    Error: Fail,
    F: Fn(State, Item) -> Fut,
    Fut: TryFuture<Ok = State, Error = Error>,
{
    func: F,
    fut: Option<Fut>,
    write: Option<State>,
    item_type: PhantomData<Item>,
}

#[derive(Debug, Fail)]
pub enum TryFoldSinkError<Error>
where
    Error: Fail,
{
    #[fail(display = "sink is busy")]
    Busy,
    #[fail(display = "polling future failed")]
    Other(#[fail(cause)] Error),
}

impl<State, Item, Error, F, Fut> TryFoldSink<State, Item, Error, F, Fut>
where
    Error: Fail,
    F: Fn(State, Item) -> Fut,
    Fut: TryFuture<Ok = State, Error = Error>,
{
    unsafe_unpinned!(func: F);
    unsafe_unpinned!(write: Option<State>);
    unsafe_pinned!(fut: Option<Fut>);
}

impl<State, Item, Error, F, Fut> Sink<Item> for TryFoldSink<State, Item, Error, F, Fut>
where
    Error: Fail,
    F: Fn(State, Item) -> Fut,
    Fut: TryFuture<Ok = State, Error = Error>,
{
    type Error = TryFoldSinkError<Error>;

    fn poll_ready(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.poll_flush(ctx)
    }

    fn start_send(mut self: Pin<&mut Self>, item: Item) -> Result<(), Self::Error> {
        if let Some(write) = self.as_mut().write().take() {
            let fut = (self.as_mut().func())(write, item);
            Pin::set(&mut self.fut(), Some(fut));

            Ok(())
        } else {
            Err(TryFoldSinkError::Busy)
        }
    }

    fn poll_flush(
        mut self: Pin<&mut Self>,
        ctx: &mut Context<'_>,
    ) -> Poll<Result<(), Self::Error>> {
        if let Some(fut) = self.as_mut().fut().as_pin_mut() {
            match fut.try_poll(ctx) {
                Poll::Ready(Ok(write)) => {
                    *self.as_mut().write() = Some(write);
                    Pin::set(&mut self.fut(), None);
                    Poll::Ready(Ok(()))
                }
                Poll::Ready(Err(error)) => Poll::Ready(Err(TryFoldSinkError::Other(error))),
                Poll::Pending => Poll::Pending,
            }
        } else {
            Poll::Ready(Ok(()))
        }
    }

    fn poll_close(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.poll_flush(ctx)
    }
}
