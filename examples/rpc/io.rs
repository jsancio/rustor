use bincode::deserialize;
use bincode::serialize;
use bincode::serialized_size;
use rustor::buffer::Buf;
use rustor::buffer::BufExt;
use rustor::buffer::BufMutExt;
use rustor::buffer::Buffer;
use rustor::buffer::ReadView;
use rustor::io::AsyncRead;
use rustor::io::AsyncReadExt;
use rustor::io::AsyncWrite;
use rustor::io::AsyncWriteExt;
use serde::Deserialize;
use serde::Serialize;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Result as IoResult;

#[derive(Debug, Serialize, Deserialize)]
pub struct Response {
    pub integer: u64,
    pub float: f64,
}

impl Response {
    pub fn new(integer: u64, float: f64) -> Response {
        Response { integer, float }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Request {
    pub integer: u64,
    pub float: f64,
}

impl Request {
    pub fn new(integer: u64, float: f64) -> Request {
        Request { integer, float }
    }
}

async fn recv_raw<R>(read: &mut R) -> Option<IoResult<(u64, Buffer)>>
where
    R: AsyncRead + Unpin,
{
    let mut meta_buffer = match read
        .read_exact(Buffer::with_capacity(16).write_view())
        .await
    {
        Ok(view) => view.buffer(),
        Err(read_error) => {
            let error = read_error.drop_state();
            if error.kind() == ErrorKind::UnexpectedEof
                || error.kind() == ErrorKind::ConnectionReset
            {
                return None;
            } else {
                return Some(Err(error));
            }
        }
    };
    let id = {
        let mut temp = [0; 8];
        meta_buffer.get_slice(&mut temp);
        u64::from_be_bytes(temp)
    };
    let size = {
        let mut temp = [0; 8];
        meta_buffer.get_slice(&mut temp);
        u64::from_be_bytes(temp)
    };

    let payload = match read
        .read_exact(Buffer::with_capacity(size as usize).write_view())
        .await
    {
        Ok(buffer) => buffer,
        Err(error) => {
            // TODO: we are dropping the buffer here
            return Some(Err(error.drop_state()));
        }
    };

    Some(Ok((id, payload.buffer())))
}

pub async fn send_raw<'a, W, P>(write: &'a mut W, id: u64, payload: &'a P) -> IoResult<Buffer>
where
    P: Serialize,
    W: AsyncWrite + Unpin,
{
    let size = serialized_size(payload).map_err(|_| {
        // TODO: fix error handling by using RPC Error
        Error::new(
            ErrorKind::Other,
            "unable to compute size of payload to send",
        )
    })?;

    let mut buffer = Buffer::with_capacity(8 + 8 + size as usize);
    buffer.put_slice(&id.to_be_bytes());
    buffer.put_slice(&size.to_be_bytes());
    // TODO: Remove this copy. The copy is needed to get around
    // `std::io::Write` is not implemented for `[std::mem::MaybeUninit<u8>]`
    let serial_payload = serialize(payload).map_err(|_| {
        // TODO: fix error handling by using RPC error
        Error::new(ErrorKind::Other, "unable to serialize payload to send")
    })?;
    buffer.put_slice(&serial_payload);

    // TODO: We are dropping the buffer
    write
        .write_all(buffer.read_view())
        .await
        .map_err(|error| error.drop_state())
        .map(ReadView::buffer)
}

pub async fn recv_response<R>(mut read: R) -> Option<(IoResult<(u64, Response)>, R)>
where
    R: AsyncRead + Unpin,
{
    recv_raw(&mut read).await.map(|result| {
        let result = result.and_then(|(id, payload)| match deserialize(payload.read_slice()) {
            Ok(response) => Ok((id, response)),
            Err(_) => Err(Error::new(
                ErrorKind::Other,
                "unable to deserialize payload recieved",
            )),
        });

        (result, read)
    })
}

pub async fn recv_request<R>(mut read: R) -> Option<(IoResult<(u64, Request)>, R)>
where
    R: AsyncRead + Unpin,
{
    let result = recv_raw(&mut read).await?;

    let result = result.and_then(|(id, payload)| match deserialize(payload.read_slice()) {
        Ok(request) => Ok((id, request)),
        Err(_) => Err(Error::new(
            ErrorKind::Other,
            "unable to deserialize payload recieved",
        )),
    });

    Some((result, read))
}

pub async fn send_response<W>(mut write: W, tuple: (u64, Response)) -> IoResult<W>
where
    W: AsyncWrite + Unpin,
{
    let (id, response) = tuple;
    send_raw(&mut write, id, &response).await?;
    Ok(write)
}

pub async fn send_request<W>(mut write: W, tuple: (u64, Request)) -> IoResult<W>
where
    W: AsyncWrite + Unpin,
{
    let (id, request) = tuple;
    send_raw(&mut write, id, &request).await?;
    Ok(write)
}
