use {
    crate::{
        io::{Request, Response},
        sink::TryFoldSinkError,
    },
    core::pin::Pin,
    futures::{
        channel::{mpsc, oneshot},
        ready,
        sink::{Sink, SinkExt},
        stream::Stream,
    },
    pin_utils::{unsafe_pinned, unsafe_unpinned},
    std::{
        collections::HashMap,
        future::Future,
        io,
        task::{Context, Poll},
    },
};

pub fn rpc<S, Si>(stream: S, sink: Si) -> (RpcHandle, RpcProcess<S, Si>) {
    let (sender, receiver) = mpsc::channel(1);

    (
        RpcHandle { sender },
        RpcProcess {
            stream,
            sink,
            request_id: 0,
            receiver,
            reply: HashMap::new(),
        },
    )
}

pub struct RpcHandle {
    sender: mpsc::Sender<(oneshot::Sender<Response>, Request)>,
}

pub async fn send(handle: &mut RpcHandle, request: Request) -> io::Result<Response> {
    let (tx, rx) = oneshot::channel();
    handle.sender.send((tx, request)).await.map_err(|err| {
        // TODO: this should be RPC errors
        let message = if err.is_full() {
            "rpc process is full while sending request"
        } else if err.is_disconnected() {
            "rpc process is disconnected while sending request"
        } else {
            "unknown error while sending request"
        };
        io::Error::new(io::ErrorKind::Other, message)
    })?;

    rx.await.map_err(|_| {
        // TODO: this should be an RPC error
        io::Error::new(
            io::ErrorKind::Other,
            "rpc process canceled while receiveing response",
        )
    })
}

pub struct RpcProcess<S, Si> {
    stream: S,
    sink: Si,
    request_id: u64,
    receiver: mpsc::Receiver<(oneshot::Sender<Response>, Request)>,
    reply: HashMap<u64, oneshot::Sender<Response>>,
}

impl<S, Si> RpcProcess<S, Si>
where
    Si: Sink<(u64, Request), Error = TryFoldSinkError<io::Error>>,
    S: Stream<Item = io::Result<(u64, Response)>>,
{
    unsafe_unpinned!(reply: HashMap<u64, oneshot::Sender<Response>>);
    unsafe_unpinned!(request_id: u64);
    unsafe_pinned!(receiver: mpsc::Receiver<(oneshot::Sender<Response>, Request)>);
    unsafe_pinned!(sink: Si);
    unsafe_pinned!(stream: S);

    fn poll_send(self: &mut Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<io::Result<bool>> {
        ready!(self
            .as_mut()
            .sink()
            .poll_ready(ctx)
            .map_err(|error| match error {
                TryFoldSinkError::Busy => io::Error::new(io::ErrorKind::Other, "sink is busy"),
                TryFoldSinkError::Other(error) => error,
            }))?;

        match ready!(self.as_mut().receiver().poll_next(ctx)) {
            Some((reply, request)) => {
                *self.as_mut().request_id() += 1;
                let id = *self.as_mut().request_id();
                self.as_mut().reply().insert(id, reply);

                match self.as_mut().sink().start_send((id, request)) {
                    Ok(()) => Poll::Ready(Ok(true)), // Continue polling
                    Err(error) => match error {
                        TryFoldSinkError::Busy => {
                            Poll::Ready(Err(io::Error::new(io::ErrorKind::Other, "sink is busy")))
                        }
                        TryFoldSinkError::Other(error) => Poll::Ready(Err(error)),
                    },
                }
            }

            None => Poll::Ready(Ok(false)), // Stop polling
        }
    }

    fn poll_recv(self: &mut Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<io::Result<bool>> {
        match ready!(self.as_mut().stream().poll_next(ctx)) {
            Some(Ok((id, response))) => {
                if let Some(reply) = self.as_mut().reply().remove(&id) {
                    // TODO: deal with this returning a Err(response)
                    let _ = reply.send(response);
                } else {
                    // TODO: We got a reply for an id that doesn't exists.
                }

                // Continue polling
                Poll::Ready(Ok(true))
            }
            Some(Err(error)) => Poll::Ready(Err(error)),
            None => Poll::Ready(Ok(false)), //Stop polling
        }
    }
}

impl<S, Si> Future for RpcProcess<S, Si>
where
    Si: Sink<(u64, Request), Error = TryFoldSinkError<io::Error>>,
    S: Stream<Item = io::Result<(u64, Response)>>,
{
    type Output = io::Result<()>;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        loop {
            let send_poll = self.poll_send(ctx)?;
            let recv_poll = self.poll_recv(ctx)?;

            if send_poll.is_pending() && recv_poll.is_pending() {
                return Poll::Pending;
            } else {
                match (send_poll, recv_poll) {
                    (Poll::Ready(false), Poll::Ready(false)) => return Poll::Ready(Ok(())),
                    (Poll::Ready(false), _) => {
                        if self.as_mut().reply().is_empty() {
                            // No more senders and no pending responses
                            return Poll::Ready(Ok(()));
                        }
                    }
                    _ => {}
                }
            }
        }
    }
}
