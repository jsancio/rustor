use core::fmt::Debug;
use core::future::Future;
use futures::future::TryFuture;
use futures::future::TryFutureExt;
use futures::stream::TryStreamExt;
use futures::task::LocalSpawn;
use futures::task::LocalSpawnExt;
use http::header::HeaderValue;
use http::header::CONTENT_LENGTH;
use http::Response;
use rustor::buffer::Buffer;
use rustor::codec::decode_http_request;
use rustor::codec::encode_http_response;
use rustor::executor::EventLoop;
use rustor::executor::Registry;
use rustor::io::AsyncReadExt;
use rustor::net::AsyncTcpListener;
use rustor::net::AsyncTcpStream;
use std::io::ErrorKind;
use std::io::Result as IoResult;

fn main() -> IoResult<()> {
    let mut event_loop = EventLoop::new()?;

    event_loop
        .spawner()
        .spawn_local(handle_error(listen(
            event_loop.registry(),
            event_loop.spawner(),
        )))
        .map_err(|_| std::io::Error::new(ErrorKind::Other, "Error spawning tcp listener"))?;

    Ok(event_loop.run()?)
}

fn handle_error<Fut, E>(fut: Fut) -> impl Future<Output = ()>
where
    E: Debug,
    Fut: TryFuture<Ok = (), Error = E>,
{
    fut.unwrap_or_else(|err| {
        eprintln!("Error: {:?}", err);
        ()
    })
}

async fn listen<T>(registry: Registry, spawner: T) -> IoResult<()>
where
    T: LocalSpawn,
{
    let mut listener = AsyncTcpListener::bind(registry, "127.0.0.1:8888")?;

    while let Some(stream) = listener.try_next().await? {
        spawner
            .spawn_local(handle_error(process(stream)))
            .map_err(|_| {
                std::io::Error::new(ErrorKind::Other, "Error spawning ping pong processor")
            })?;
    }

    Ok(())
}

async fn process(stream: AsyncTcpStream) -> IoResult<()> {
    let (mut reader, mut writer) = stream.split();
    let mut buffer = Buffer::with_capacity(1024 * 1024);
    let mut header_buffer = Buffer::with_capacity(1024);

    while let Some(request) = decode_http_request(&mut reader, buffer).await? {
        let (request_parts, (new_buffer, body)) = request.into_parts();
        buffer = new_buffer;

        let response = Response::builder()
            .header(
                CONTENT_LENGTH,
                request_parts
                    .headers
                    .get(CONTENT_LENGTH)
                    .unwrap_or(&HeaderValue::from_static("0")),
            )
            .body((buffer, body))
            .map_err(|_| std::io::Error::new(ErrorKind::Other, "Error creating HTTP response"))?;

        let (new_header_buffer, new_buffer) =
            encode_http_response(&mut writer, header_buffer, response).await?;
        header_buffer = new_header_buffer;
        buffer = new_buffer;
    }

    Ok(())
}
