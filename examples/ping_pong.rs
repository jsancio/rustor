#![feature(arbitrary_self_types)]

use core::fmt::Debug;
use core::future::Future;
use core::pin::Pin;
use core::task::Context;
use core::task::Poll;
use futures::future::TryFuture;
use futures::future::TryFutureExt;
use futures::ready;
use futures::sink::Sink;
use futures::stream::Stream;
use futures::stream::StreamExt;
use futures::stream::TryStreamExt;
use futures::task::LocalSpawn;
use futures::task::LocalSpawnExt;
use rustor::buffer::Buf;
use rustor::buffer::BufExt;
use rustor::buffer::BufMut;
use rustor::buffer::BufMutExt;
use rustor::buffer::Buffer;
use rustor::executor::EventLoop;
use rustor::executor::Registry;
use rustor::io::AsyncRead;
use rustor::io::AsyncWrite;
use rustor::net::AsyncTcpListener;
use rustor::net::AsyncTcpStream;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Result as IoResult;

/* This process can be used against redis-benchmark. To run the TCP listener:
 *
 * cargo +nightly run --release --example ping_pong
 *
 * To run the benchmark:
 *
 * redis-benchmark -t ping_inline -n  1000000 -P 1 -p 8888
 */

fn main() -> IoResult<()> {
    let mut event_loop = EventLoop::new()?;

    event_loop
        .spawner()
        .spawn_local(handle_error(listen(
            event_loop.registry(),
            event_loop.spawner(),
        )))
        .map_err(|_| Error::new(ErrorKind::Other, "Error spawning tcp listener"))?;

    Ok(event_loop.run()?)
}

fn handle_error<Fut, E>(fut: Fut) -> impl Future<Output = ()>
where
    E: Debug,
    Fut: TryFuture<Ok = (), Error = E>,
{
    fut.unwrap_or_else(|err| {
        eprintln!("Error: {:?}", err);
        ()
    })
}

async fn listen<T: LocalSpawn>(registry: Registry, spawner: T) -> IoResult<()> {
    let mut listener = AsyncTcpListener::bind(registry, "127.0.0.1:8888")?;

    while let Some(stream) = listener.try_next().await? {
        spawner
            .spawn_local(handle_error(process(stream)))
            .map_err(|_| Error::new(ErrorKind::Other, "Error spawning ping pong processor"))?;
    }

    Ok(())
}

fn process(stream: AsyncTcpStream) -> impl Future<Output = IoResult<()>> {
    let (sink, stream) = Ping::new(stream).split();

    stream.forward(sink).map_ok(|_| ())
}

struct Ping {
    in_buffer: Option<Buffer>,
    out_buffer: Option<Buffer>,
    stream: AsyncTcpStream,
}

impl Ping {
    fn new(stream: AsyncTcpStream) -> Self {
        Ping {
            in_buffer: Some(Buffer::with_capacity(8 * 1024)),
            out_buffer: Some(Buffer::with_capacity(8 * 1024)),
            stream,
        }
    }
}

enum Command {
    Ping,
}

impl Stream for Ping {
    type Item = IoResult<Vec<Command>>;

    fn poll_next(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let this = self.get_mut();
        let ping = b"PING\r\n";

        loop {
            let ready_result = ready!(this.stream.poll_read_ready(ctx)).or_else(|err| {
                if err.error().kind() == ErrorKind::ConnectionReset {
                    Ok(Some((0, err.drop_error())))
                } else {
                    // TODO: I don't think is correct. I think we need to save the buffer.
                    Err(err.drop_state())
                }
            })?;

            // TODO: This seems to be a common pattern. ReadExact does the same thing. Figure out a
            // way to abstract this. Or, take a look at how we implemented WriteAll and see we can
            // use that pattern instead.
            let mut buffer = match (ready_result, this.in_buffer.take()) {
                (Some((size, write_view)), None) => {
                    if size == 0 {
                        return Poll::Ready(None);
                    } else {
                        write_view.buffer()
                    }
                }
                (None, Some(buffer)) => buffer,
                (Some(_), Some(_)) => panic!("Read returned an unexpected buffer"),
                (None, None) => panic!("Read didn't return an expected buffer"),
            };

            let mut commands = Vec::new();
            while buffer.read_slice().len() >= ping.len() {
                if ping == &buffer.read_slice()[..ping.len()] {
                    buffer.advance_read_index(ping.len());

                    commands.push(Command::Ping);
                } else {
                    return Poll::Ready(Some(Err(Error::new(ErrorKind::Other, "Unknown command"))));
                }
            }

            if !commands.is_empty() {
                this.in_buffer = Some(buffer);
                return Poll::Ready(Some(Ok(commands)));
            } else {
                buffer.discard_read();

                // TODO: Really? We need to clean this up some way some how. This is too verbose
                let result = ready!(this.stream.poll_read(ctx, buffer.write_view()))
                    .map(|(size, write_view)| {
                        if size == 0 {
                            None
                        } else {
                            Some(write_view.buffer())
                        }
                    })
                    .or_else(|err| {
                        if err.error().kind() == ErrorKind::ConnectionReset {
                            Ok(None)
                        } else {
                            // TODO: I don't think is correct. I think we need to save the buffer.
                            Err(err.drop_state())
                        }
                    })?;

                if let Some(buffer) = result {
                    this.in_buffer = Some(buffer);
                } else {
                    return Poll::Ready(None);
                }
            }
        }
    }
}

impl Sink<Vec<Command>> for Ping {
    type Error = Error;

    fn poll_ready(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        let this = self.get_mut();

        loop {
            let ready_result = ready!(this.stream.poll_write_ready(ctx))
                .map_err(|error| error.drop_state())?
                .map(|(_, read_view)| read_view.buffer());

            if ready_result.is_some() && this.out_buffer.is_some() {
                panic!("Error writing, poll_write_ready returned an unexpected buffer");
            }

            match this.out_buffer.take().or(ready_result) {
                Some(buffer) => {
                    if buffer.is_readable() {
                        let (_, read_view) = ready!(this
                            .stream
                            .poll_write(ctx, buffer.read_view())
                            .map_err(|error| error.drop_state()))?;
                        this.out_buffer = Some(read_view.buffer());
                    } else {
                        this.out_buffer = Some(buffer);
                        break Poll::Ready(Ok(()));
                    }
                }
                None => panic!("Error writing, poll_write_ready didn't return an expected buffer"),
            }
        }
    }

    fn start_send(self: Pin<&mut Self>, item: Vec<Command>) -> Result<(), Self::Error> {
        let this = self.get_mut();

        if let Some(mut buffer) = this.out_buffer.take() {
            let pong = b"+PONG\r\n";
            for _ in item {
                if buffer.write_mut_slice().len() < pong.len() {
                    buffer.discard_read();
                }
                buffer.put_slice(pong);
            }
            this.out_buffer = Some(buffer);

            Ok(())
        } else {
            Err(Error::new(
                ErrorKind::Other,
                "Error calling start_send when Ping had pending write",
            ))
        }
    }

    fn poll_flush(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.poll_ready(ctx)
    }

    fn poll_close(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.poll_ready(ctx)
    }
}
